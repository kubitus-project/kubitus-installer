__metaclass__ = type

DOCUMENTATION = r'''
  lookup: extract_gitops_instance_variable
  author: Mathieu Parent
  short_description: extract GitOps instance variables
  description:
    - Extract GitOps instance variables.
'''

EXAMPLES = r'''
    instance: "{{ lookup('extract_gitops_instance_variable').instance }}"
    #  description: 'Foo'
'''

RETURN = r'''
  _value:
    description:
      - A dictionary with variable name as key and variable value as value :-).
    type: any
'''

from ansible.errors import AnsibleError, AnsibleUndefinedVariable
from ansible.plugins.lookup import LookupBase
from ansible.vars.hostvars import STATIC_VARS


class LookupModule(LookupBase):
    '''Extract GitOps instance variables filter.'''

    def run(
        self,
        terms,
        variables: dict,
    ):
        # Setup templar
        self._templar.available_variables = variables
        static_vars = STATIC_VARS + ['gitops_instance_variables']

        # Application
        try:
            gitops_name = self._templar.template(variables['gitops_name'], fail_on_undefined=True)
        except KeyError:
            raise AnsibleError('Variable gitops_name must be set')
        gitops_slug = gitops_name.replace('-', '_')

        # Instance
        try:
            gitops_instance = self._templar.template(variables['gitops_instance'], fail_on_undefined=True)
        except KeyError:
            raise AnsibleError('Variable gitops_instance must be set')
        gitops_instance_suffix = '' if gitops_instance == 'default' else ('-' + gitops_instance)
        gitops_instance_name = gitops_name + gitops_instance_suffix
        gitops_instance_slug = (gitops_name + '-instance-' + gitops_instance).replace('-', '_')

        # Loop over variables
        # defaults
        gitops_instance_variables = {
            'suffix': gitops_instance_suffix,
            'name': gitops_instance_name,
            'slug': gitops_instance_slug,
            'role': gitops_slug,
            'description': gitops_name.capitalize(),
            'cascading_deletion': 'foreground',
            'namespace': gitops_name,
            'create_namespace': True,
            'namespace_pod_security': 'baseline',
            'extra_namespaces': [],
            'helm_dependencies': [],
            'ignore_differences': [],
            'keep_file_paths': [],
            'helm_skip_crds': False,
            'patches_json6902': [],
            'patches_strategic_merge': [],
            'server_side_apply': False,
            'server_side_diff': False,
            'syncpolicy_automated': self._templar.template("{{ argocd_application_syncpolicy_automated }}", fail_on_undefined=True, static_vars=static_vars),
            'extra_cluster_resource_whitelist': [],
            'extra_namespace_resource_whitelist': [],
            'ingress_class': self._templar.template("{{ kubitus_ingress_class }}", fail_on_undefined=True, static_vars=static_vars),
            'sealed_secrets': [],
            'tenant': self._templar.template("{{ kubitus_tenant }}", fail_on_undefined=True, static_vars=static_vars),
        }
        self._templar.available_variables['gitops_instance_variables'] = gitops_instance_variables
        existing_gitops_slugs = [
            # argocd
            'argocd_apps',
            # gitlab
            'gitlab_runner',
            # kubespray
            'kubespray_prerequisites'
            # kubitus
            'kubitus_defaults',
            # minio
            'minio_operator',
            'minio_tenant',
        ]
        try:
            existing_gitops_slugs.remove(gitops_slug)
        except ValueError:
            pass
        variables_retry = []
        # shared override
        for name, value in variables.items():
            if (name.startswith(gitops_slug + '_') and
                not name.startswith(gitops_slug + '_instance_') and
                not any(name.startswith(existing_gitops_slug+'_') for existing_gitops_slug in existing_gitops_slugs)
            ):
                shortname = name[len(gitops_slug)+1:]
                try:
                    gitops_instance_variables[shortname] = self._templar.template(value, fail_on_undefined=True, static_vars=static_vars)
                except AnsibleUndefinedVariable:
                    variables_retry.append(shortname)
        # instance defaults
        gitops_instance_variables['description'] = '{0} {1}'.format(gitops_instance_variables['description'], gitops_instance)
        gitops_instance_variables['namespace'] = '{0}{1}'.format(gitops_instance_variables['namespace'], gitops_instance_suffix)
        gitops_instance_variables['extra_namespaces'] = ['{0}{1}'.format(namespace, gitops_instance_suffix) for namespace in gitops_instance_variables['extra_namespaces']]
        # instance overrides
        for name, value in variables.items():
            if name.startswith(gitops_instance_slug + '_'):
                shortname = name[len(gitops_instance_slug)+1:]
                try:
                    gitops_instance_variables[shortname] = self._templar.template(value, fail_on_undefined=True, static_vars=static_vars)
                except AnsibleUndefinedVariable:
                    variables_retry = shortname
        for shortname in variables_retry:
                gitops_instance_variables[shortname] = self._templar.template(
                    variables.get(gitops_instance_slug + '_' + shortname, variables.get(gitops_slug + '_' + shortname)),
                    fail_on_undefined=True,
                    static_vars=static_vars,
                )
        gitops_instance_variables['_sealed_secrets'] = {}
        for sealed_secret in gitops_instance_variables['sealed_secrets']:
            gitops_instance_variables['_sealed_secrets'][sealed_secret] = {}
            sealed_secret_slug = sealed_secret.replace('-', '_')
            gitops_instance_variables['_sealed_secrets'][sealed_secret]['path'] = 'templates/sealedsecret-{0}.yaml'.format(
                sealed_secret_slug,
            )
            sealed_secret_prefix = 'sealed_secret_{0}'.format(sealed_secret_slug)
            gitops_instance_variables['{0}_name'.format(sealed_secret_prefix)] = gitops_instance_variables.get(
                '{0}_name'.format(sealed_secret_prefix),
                sealed_secret,
            )
            gitops_instance_variables['_sealed_secrets'][sealed_secret]['name'] = gitops_instance_variables['{0}_name'.format(sealed_secret_prefix)]
            gitops_instance_variables['{0}_namespace'.format(sealed_secret_prefix)] = gitops_instance_variables.get(
                '{0}_namespace'.format(sealed_secret_prefix),
                gitops_instance_variables['namespace'],
            )
            gitops_instance_variables['_sealed_secrets'][sealed_secret]['namespace'] = gitops_instance_variables['{0}_namespace'.format(sealed_secret_prefix)]
            gitops_instance_variables['{0}_labels'.format(sealed_secret_prefix)] = gitops_instance_variables.get(
                '{0}_labels'.format(sealed_secret_prefix),
                {},
            )
            gitops_instance_variables['_sealed_secrets'][sealed_secret]['labels'] = gitops_instance_variables['{0}_labels'.format(sealed_secret_prefix)]
            gitops_instance_variables['{0}_type'.format(sealed_secret_prefix)] = gitops_instance_variables.get(
                '{0}_type'.format(sealed_secret_prefix),
                self._templar.template("{{ omit }}", fail_on_undefined=True, static_vars=static_vars),
            )
            gitops_instance_variables['_sealed_secrets'][sealed_secret]['type'] = gitops_instance_variables['{0}_type'.format(sealed_secret_prefix)]
            gitops_instance_variables['{0}_unencrypted_data'.format(sealed_secret_prefix)] = gitops_instance_variables.get(
                '{0}_unencrypted_data'.format(sealed_secret_prefix),
                {},
            )
            gitops_instance_variables['_sealed_secrets'][sealed_secret]['unencrypted_data'] = gitops_instance_variables['{0}_unencrypted_data'.format(sealed_secret_prefix)]
            gitops_instance_variables['{0}_items'.format(sealed_secret_prefix)] = gitops_instance_variables.get('{0}_items'.format(sealed_secret_prefix), [])
            gitops_instance_variables['{0}_data'.format(sealed_secret_prefix)] = gitops_instance_variables.get(
                '{0}_data'.format(sealed_secret_prefix),
                {},
            )
            gitops_instance_variables['_sealed_secrets'][sealed_secret]['data'] = gitops_instance_variables['{0}_data'.format(sealed_secret_prefix)]
            for item in gitops_instance_variables['{0}_items'.format(sealed_secret_prefix)]:
                item_slug = item.lower().replace('-', '_').replace('.', '_')
                item_prefix = '{0}_data_{1}'.format(sealed_secret_prefix, item_slug)
                gitops_instance_variables['{0}_key'.format(item_prefix)] = gitops_instance_variables.get(
                    '{0}_key'.format(item_prefix),
                    item,
                )
                gitops_instance_variables['{0}_password_length'.format(item_prefix)] = gitops_instance_variables.get(
                    '{0}_password_length'.format(item_prefix),
                    64,
                )
                gitops_instance_variables['{0}_password_chars'.format(item_prefix)] = gitops_instance_variables.get(
                    '{0}_password_chars'.format(item_prefix),
                    'ascii_letters,digits',
                )
                gitops_instance_variables['{0}_password_encrypt'.format(item_prefix)] = gitops_instance_variables.get(
                    '{0}_password_encrypt'.format(item_prefix),
                    '',
                )
                gitops_instance_variables['{0}_value'.format(item_prefix)] = gitops_instance_variables.get(
                    '{0}_value'.format(item_prefix),
                    self._templar.template(
                        "{{{{ lookup('password', kubitus_credentials_dir + '/{0}_{1}.creds length={2} chars={3} encrypt={4}') }}}}".format(
                            gitops_instance_slug,
                            item_prefix,
                            gitops_instance_variables['{0}_password_length'.format(item_prefix)],
                            gitops_instance_variables['{0}_password_chars'.format(item_prefix)],
                            gitops_instance_variables['{0}_password_encrypt'.format(item_prefix)],
                        ),
                        fail_on_undefined=True,
                    )
                )
                gitops_instance_variables['_sealed_secrets'][sealed_secret]['data'][gitops_instance_variables['{0}_key'.format(item_prefix)]] = gitops_instance_variables['{0}_value'.format(item_prefix)]

        return [gitops_instance_variables]
