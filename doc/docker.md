# Docker

## Scope

The `docker` role installs [Docker](https://mobyproject.org/).

It's a dependency of the following roles:

- [GitLabracadabra](gitlabracadabra.md)
- [kind](kind.md)

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/docker.yml).

Public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `docker_channel` | Docker channel. Either [`debian`](https://packages.debian.org/stable/docker.io), or one of [`stable`](https://docs.docker.com/engine/install/debian/#set-up-the-repository), [`test`](https://docs.docker.com/engine/install/debian/#install-pre-releases) or `nightly` | `debian` |
| `docker_package` | Docker package | `"{% if docker_channel == 'debian' %}docker.io{% else %}docker-ce{% endif %}"` |
