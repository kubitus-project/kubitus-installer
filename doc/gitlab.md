# GitLab

## Scope

The `gitlab` role installs GitLab out of the cluster, used to store artifacts
(synced with [GitLabracadabra](gitlabracadabra.md)) for offline mode.

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/gitlab.yml).

Public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `gitlab_external_enabled` | Enable GitLab | When the `gitlab` group is not empty: `"{{ groups['gitlab'] \| default([]) \| length > 0 }}"` |
| `gitlab_nftables_address_family` | `nftables` address family name. Valid values: `inet` or `ip` (IPv4) | `inet` |
| `gitlab_nftables_table_name` | `nftables` table name | `firewall` |
| `gitlab_nftables_input_chain_name` | `nftables` INPUT chain name | `input` |
| `gitlab_prometheus_allowed_ips` | List of IPs allowed to access Prometheus | `[]` |
| `gitlab_fqdn` | GitLab DNS name | `"gitlab.{{ top_level_domain }}"` |
| `gitlab_registry_fqdn` | GitLab registryDNS name | `"gitlab-registry.{{ top_level_domain }}"` |
| `gitlab_users` | GitLab Users to create | :page_with_curl: |
| `gitlab_force_refresh_tokens` | re-run the task *Destroy root and kubitus users, and create users for Kubitus*, which create tokens | `false` |
| `gitlab_application_settings` | Application settings | `{signup_enabled: false}` |
| `gitlab_debian_repo_base_url` | URL of the Debian repository | `https://packages.gitlab.com/gitlab/gitlab-ce/debian/` |
| `gitlab_install_mode` | GitLab installation mode. One of `apt` or `deb` | `apt` |
| `gitlab_deb_url` | GitLab URL when `gitlab_install_mode` is `deb` | `"{{ gitlab_debian_repo_base_url }}/pool/{{ ansible_facts.distribution_release }}/main/g/gitlab-ce/gitlab-ce_{{ gitlab_version }}_amd64.deb"` |
| `gitlab_version` | GitLab version | :page_with_curl: |
