# CSI Snapshotter

## Scope

The `csi_snapshotter` role installs [external-snapshotter](https://github.com/kubernetes-csi/external-snapshotter)'s components:

- Kubernetes Volume Snapshot CRDs
- Volume snapshot controller
- Snapshot validation webhook

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/csi_snapshotter.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `csi_snapshotter_enabled` | Enable CSI Snapshotter | `false` |
| `csi_snapshotter_description` | Description | `CSI Snapshotter` |
| `csi_snapshotter_helm_dependencies` | Helm dependencies | :page_with_curl: |

Artefacts variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `csi_snapshotter_controller_chart_version` |  | :page_with_curl: |
| `csi_snapshotter_controller_image` |  | :page_with_curl: |
| `csi_snapshotter_webhook_image` |  | :page_with_curl: |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `csi_snapshotter_projects` |  | :page_with_curl: |
