# Prom Label Proxy

## Scope

The `prom-label-proxy` role installs [prom-label-proxy](https://github.com/prometheus-community/prom-label-proxy/), a proxy that enforces a given label in a given PromQL query..

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/prom_label_proxy.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `prom_label_proxy_enabled` | Enable Prom Label Proxy | `"{{ tenants and prometheus_stack_enabled }}"` |
| `prom_label_proxy_description` | Description | `Prom Label Proxy` |
| `prom_label_proxy_helm_dependencies` | Helm dependencies | :page_with_curl: |

Config variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `prom_label_proxy_kube_rbac_proxy_enabled` | Enable kubeRBACProxy | `false` |

Resources variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `prom_label_proxy_memory_request` |  | `64Mi` |
| `prom_label_proxy_cpu_request` |  | `100m` |
| `prom_label_proxy_memory_limit` |  | `128Mi` |
| `prom_label_proxy_cpu_limit` |  | `200Mi` |

Artifacts variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `prom_label_proxy_chart_version` |  | :page_with_curl: |
| `prom_label_proxy_image` | | :page_with_curl: |
| `prom_label_proxy_kube_rbac_proxy_image` | | :page_with_curl: |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `prom_label_proxy_projects` |  | :page_with_curl: |
