# ArgoCD

## Scope

The `selfcheck` role test the health and sync status of installed applications.

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/selfcheck.yml).

Public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `selfcheck_expected_sync_statuses` | Map of application name to list of expected sync statuses (defaults to `[Synced]`) | `{}` |
| `selfcheck_expected_health_statuses` | Map of application name to list of expected health statuses (defaults to `[Healthy]`) | `{}` |
| `selfcheck_retries` | Number of retries when application is not in the expected state | `3` |
| `selfcheck_delay` | Delay between retries | `10` |
