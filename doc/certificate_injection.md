# Certificate injection

## Scope

This role takes the local certificates from the Kubitus node
(in `/usr/local/share/ca-certificates`), and bundle this in a ConfigMap,
which will be mounted as `/etc/ssl/certs/ca-certificates.crt`
in pods matching the following:

```yaml
    namespaceSelector:
      matchExpressions:
      - key: kubitus-project.gitlab.io/inject-ca-bundle
        operator: NotIn
        values:
        - 'false'
    labelSelector:
      matchExpressions:
      - key: kubitus-project.gitlab.io/inject-ca-bundle-debian
        operator: In
        values:
        - 'true'
```

The same mechanism is available for Java keystore, mounted as `/etc/ssl/certs/java/cacerts` and
`/opt/java/openjdk/lib/security/cacerts`
in pods matching the following:

```yaml
    namespaceSelector:
      matchExpressions:
      - key: kubitus-project.gitlab.io/inject-ca-bundle
        operator: NotIn
        values:
        - 'false'
    labelSelector:
      matchExpressions:
      - key: kubitus-project.gitlab.io/inject-ca-bundle-java
        operator: In
        values:
        - 'true'
```

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/certificate_injection.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `certificate_injection_enabled` | Enable Certificate injection. :warning: `cert-manager` and `trust-manager` shoul be installed | `false` |
| `certificate_injection_extra_cluster_resource_whitelist` | Extra cluster resource whitelist | :page_with_curl: |
