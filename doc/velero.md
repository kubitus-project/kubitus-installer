# Velero

## Scope

The `velero` role installs [Velero](https://velero.io/), as backup software.

## Schedules

```yaml
velero_schedules:
  # all resources except PVs, six times a day
  resources:
    schedule: '0 9,11,13,15,17,19 * * *'
    useOwnerReferencesInBackup: true
    template:
      ttl: "{{ 31 * 24 }}h"
      excludedClusterScopedResources:
      - persistentvolumes
```

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/velero.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `velero_enabled` | Enable Velero | `false` |
| `velero_helm_dependencies` | Helm dependencies | :page_with_curl: |
| `velero_extra_namespace_resource_whitelist` | Extra namespace resource whitelist | :page_with_curl: |
| `velero_sealed_secrets` | Sealed Secrets | `[velero]` |
| `velero_sealed_secret_velero_items` | `velero` secret items | `[cloud]` |
| `velero_sealed_secret_velero_data_cloud_value` | `cloud` item content | :page_with_curl: |

Other public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `velero_provider` | `BackupStorageLocation` and `VolumeSnapshotLocation` provider | `aws` |
| `velero_backup_name` | `BackupStorageLocation` name | `default` |
| `velero_backup_bucket` | `BackupStorageLocation` bucket | `velero` |
| `velero_backup_ca_cert` | CA bundle to use when verifying TLS connections to the provider | `"{{ lookup('file', pki_dir + '/intermediate_ca_signed.pem') }}"` |
| `velero_backup_region` | `BackupStorageLocation` S3 region | `minio` |
| `velero_backup_s3_url` | `BackupStorageLocation` S3 URL | `https://{{ minio_fqdn }}` |
| `velero_backup_checksum_algorithm` | `checksumAlgorithm` for `PutObject` to S3. `null` to use plugin default. Use `""` to disable | `null` |
| `velero_aws_access_key_id` | S3 access key | :page_with_curl: |
| `velero_aws_secret_access_key` | S3 secret key | :page_with_curl: |
| `velero_volume_snapshot_name` | `VolumeSnapshotLocation` name | `default` |
| `velero_volume_snapshot_region` | `VolumeSnapshotLocation` S3 region | `minio` |
| `velero_csi_enabled` | Enable Container Storage Interface (CSI) snapshot support | `"{{ csi_snapshotter_enabled }}"` |
| `velero_timezone` | Timezone of velero Pod, used for schedules | `Europe/Paris` |
| `velero_schedules` | Schedules, see [above](#schedules) | `{}` |
| `velero_cpu_request` |  | 500m |
| `velero_memory_request` |  | 128Mi |
| `velero_cpu_limit` |  | 1000m |
| `velero_memory_limit` |  | 4Gi |
| `velero_chart_version` |  | :page_with_curl: |
| `velero_cli_version` |  | :page_with_curl: |
| `velero_image` |  | :page_with_curl: |
| `velero_plugin_for_aws_image` |  | :page_with_curl: |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `velero_projects` |  | :page_with_curl: |
| `velero_downloads` |  | :page_with_curl: |
