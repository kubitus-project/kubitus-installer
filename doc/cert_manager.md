# cert-manager

## Scope

The `cert_manager` role installs [cert-manager](https://cert-manager.io/),
to create and sign certificates (mainly for ingresses).

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/cert_manager.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `cert_manager_enabled` | Enable cert-manager | `true` |
| `cert_manager_extra_namespaces` | Extra namespaces | `[kube-system]` |
| `cert_manager_helm_dependencies` | Helm dependencies | :page_with_curl: |
| `cert_manager_extra_cluster_resource_whitelist` | Extra cluster resource whitelist | :page_with_curl: |
| `cert_manager_sealed_secrets` | Sealed secrets | `[ca-key-pair]` |
| `cert_manager_sealed_secret_ca_key_pair_type` |  `ca-key-pair` secret type | `kubernetes.io/tls` |
| `cert_manager_sealed_secret_ca_key_pair_items` | `ca-key-pair` secret items | `[tls.crt, tls.key]` |
| `cert_manager_sealed_secret_ca_key_pair_data_tls_key_value` | `tls.key` content | `"{{ lookup('file', pki_dir + '/intermediate_ca-key.pem') }}"` |
| `cert_manager_sealed_secret_ca_key_pair_data_tls_crt_value` | `tls.crt` content | `"{{ lookup('file', pki_dir + '/intermediate_ca_signed.pem') }}"` |

Other public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `cert_manager_ingress_shim_enabled` | Enable [`ingress-shim`](https://cert-manager.io/docs/usage/ingress/) | `"{{ ingress_nginx_enabled }}"` |
| `cert_manager_chart_version` | | :page_with_curl: |
| `cert_manager_controller_image` | | :page_with_curl: |
| `cert_manager_webhook_image` | | :page_with_curl: |
| `cert_manager_cainjector_image` | | :page_with_curl: |
| `cert_manager_acmesolver_image` | | :page_with_curl: |
| `cert_manager_startupapicheck_image` | | :page_with_curl: |
| `cert_manager_controller_cpu_request` | | `10m` |
| `cert_manager_controller_memory_request` | | `32Mi` |
| `cert_manager_controller_cpu_limit` | | `30m` |
| `cert_manager_controller_memory_limit` | | `48Mi` |
| `cert_manager_webhook_cpu_request` | | `10m` |
| `cert_manager_webhook_memory_request` | | `32Mi` |
| `cert_manager_webhook_cpu_limit` | | `30m` |
| `cert_manager_webhook_memory_limit` | | `32Mi` |
| `cert_manager_cainjector_cpu_request` | | `10m` |
| `cert_manager_cainjector_memory_request` | | `32Mi` |
| `cert_manager_cainjector_cpu_limit` | | `30m` |
| `cert_manager_cainjector_memory_limit` | | `96Mi` |
| `cert_manager_startupapicheck_cpu_request` | | `10m` |
| `cert_manager_startupapicheck_memory_request` | | `32Mi` |
| `cert_manager_startupapicheck_cpu_limit` | | `30m` |
| `cert_manager_startupapicheck_memory_limit` | | `40Mi` |


Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `cert_manager_projects` |  | :page_with_curl: |
