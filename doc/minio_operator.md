# MinIO Operator

## Scope

The `minio_operator` role installs [MinIO](min.io/) operator.
Tenants are created in the [`minio-tenant`](minio_tenant) role.

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/minio_operator.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `minio_operator_enabled` | Enable MinIO Operator | `"{{ minio_tenant_enabled }}"` |
| `minio_operator_description` | Description | `MinIO Operator` |
| `minio_operator_helm_dependencies` | Helm dependencies | :page_with_curl: |

Resources variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `minio_operator_cpu_request` | [Cpu request](https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/) | `100m` |
| `minio_operator_cpu_limit` | [Cpu limit](https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/) | `200m` |
| `minio_operator_memory_request` | [Memory request](https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/) | `64Mi` |
| `minio_operator_memory_limit` | [Memory limit](https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/) | `256Mi` |

Artefacts variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `minio_operator_chart_version` |  | :page_with_curl: |
| `minio_operator_image` |  | :page_with_curl: |
| `minio_operator_sidecar_image` |  | :page_with_curl: |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `minio_operator_projects` |  | :page_with_curl: |
