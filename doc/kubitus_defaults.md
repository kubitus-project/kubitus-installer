# Kubitus defaults

## Scope

The `kubitus_defaults` has no tasks, it only holds common variables.

## NetworkPolicies to external

When some components needs to access external services, you may need to
declare the IPs of these services. Those IPs will be used in relevant
NetworkPolicies.

Example:

```yaml
grafana_oidc_enabled: true
grafana_oidc_url: "https://auth.example.org/realms/my-realm"
#grafana_oidc_client_id: ...
#grafana_oidc_client_secret: ...

netpol_extra_fqdn2ips:
  auth.example.org:
  - 10.20.30.40
  - 10.21.31.41
```

## Tenants

Example:

```yaml
tenants:
- name: infra
  namespaces:
  - argocd
  - cilium-test
  - default
  - gatekeeper
  - grafana
  - ingress-nginx
  - kube-bench
  - kube-node-lease
  - kube-public
  - kube-state-metrics
  - kube-system
  - loki
  - metallb-system
  - minio
  - prometheus-adapter
  - prometheus-node-exporter
  - prometheus-stack
  - sealed-secrets
  - trivy-system
  - vsphere-csi
- name: ci
  namespaces:
  - gitlab-runner
```

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/kubitus_defaults.yml).

Important public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `top_level_domain` | Top level domain | `k8s.example.org` |
| `default_ingress_class` | Default ingress class | `nginx` |
| `kubitus_ingress_class` | Default ingress class for components installed by Kubitus Installer | `"{{ default_ingress_class }}"` |
| `default_ingress_certificate_issuer` | Default ingress certificate issuer. Possible values: `ca-issuer` for certificates signed by the intermediate CA of the [PKI](pki.md) using [cert-manager](cert_manager.md); `"none"` to use none (i.e. use the default certificate) | `"{% if cert_manager_enabled %}ca-issuer{% else %}none{% endif %}"` |
| `apiserver_fqdn` | Kubernetes API server name | `"apiserver.{{ top_level_domain }}"` |
| `apiserver_ingress_enabled` | Create an `Ingress` for API server (port 443). Note: the config is injected in the [ArgoCD](argocd.md) role | `false` |
| `apiserver_ingress_class` | Ingress class for the APIserver | `"{{ kubitus_ingress_class }}"` |
| `apiserver_ingress_certificate_issuer` | API server ingress certificate issuer. See above `default_ingress_certificate_issuer` for possible values | `"{{ default_ingress_certificate_issuer }}"` |
| `apiserver_ingress_proxy_read_timeout` | API server ingress read timeout | `2h` |
| `offline_enabled` | Offline mode enabled | `"{{ gitlabracadabra_external_enabled }}"` |
| `sync_windows_binaries` | Sync Windows binaries to GitLab (when Offline mode is enabled) | `false` |
| `github_token` | GitHub token, as [GitHub uses rate limiting to control API traffic](https://docs.github.com/en/rest/overview/resources-in-the-rest-api?apiVersion=2022-11-28#rate-limiting) | `''` |
| `netpol_extra_fqdn2ips` | | `{}` |
| `netpol_fqdn2ips` | | `"{{ netpol_default_fqdn2ips \| combine(netpol_extra_fqdn2ips) }}"` |
| `kube_pod_security_default_enforce` | Default pod-security | `baseline` |

Public variables for `kubectl` and other CLIs:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `kubeconfig` | `$KUBECONFIG` | `"{{ kubespray_inventory_dir }}/kubespray-artifacts/admin.conf"` |
| `kubectl` | `kubectl` binary | `"{{ kubespray_inventory_dir }}/kubespray-artifacts/kubectl"` when `kubectl_delegate_to` is a `kubespray` host, or `kubectl` otherwise |
| `kubectl_delegate_to` | Host where `kubectl` is run (and `helm`, `kubeseal`, ...), GitOps repo is | The first host in `kubespray` or `gitops` group, or `localhost` |
| `kubectl_become` | `become` for `kubectl` and GitOps related commands | `true` |
| `kubectl_user` | User for `kubectl` and GitOps related commands and files. If value is `kubitus`, the user is created | `kubitus` |
| `kubectl_group` | Group for `kubectl` and GitOps related files | `kubitus` |


Public variables for authenticating Kubernetes API requests [using OpenID Connect](https://kubernetes.io/docs/reference/access-authn-authz/authentication/#configuring-the-api-server):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `kube_oidc_auth` | | `"{{ keycloak_enabled }}"` |
| `kube_oidc_url` | | `"https://{{ keycloak_fqdn }}/auth/realms/default"` |
| `kube_oidc_client_id` | | `kubernetes` |
| `kube_oidc_username_claim` | | `preferred_username` |
| `kube_oidc_username_prefix` | | `oidc-usr-` |
| `kube_oidc_groups_claim` | | `groups` |
| `kube_oidc_groups_prefix` | | `oidc-grp-` |


Other public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `external_registries_default` | Default external registries (see `external_registries`) | :page_with_curl: |
| `external_registries_extra` | Extra external registries (see `external_registries`) | `[]` |
| `external_registries` | External registries (containerd will query external registry mirrors first for those, see `external_registry_mirror_patterns` and `containerd_registries`) | `"{{ external_registries_default + external_registries_extra }}"` |
| `external_registry_mirror_patterns` | Array of URL patterns for registry mirrors, where `$external_registry` is replaced by an item of `external_registries` (and `$external_registry_slug` is replaced by `$external_registry`, with all non alpha-numeric replaced by dash `-`) | `["https://{{ gitlab_registry_fqdn }}/v2/external-registries/$external_registry"]` when `offline_enabled`, `[]` otherwise |
| `containerd_registries` | Map of registry to array of URLs | :page_with_curl: |
| `kube_service_addresses` | Subnet for cluster IPs | `10.233.0.0/18` |
| `kube_pods_subnet` | Subnet for Pod IPs | `10.233.64.0/18` |
| `kube_network_node_prefix` | Subnet allocated per-node for pod IPs | 24` |
| `kube_controller_terminated_pod_gc_threshold` | Number of terminated pods that can exist before the terminated pod garbage collector starts deleting terminated pods | `100` |
| `kube_dns_stub_domains` | `kube-dns` `stubDomains` (see [here](https://cloud.google.com/kubernetes-engine/docs/how-to/kube-dns#adding_custom_resolvers_for_stub_domains)). Note: the config is injected in the [ArgoCD](argocd.md) role | `{}` |
| `cni_bin_dir` | CNI bin directory | `"{% if terraform_gke_enabled %}/home/kubernetes/bin{% else %}/opt/cni/bin{% endif %}"` |
| `download_cache_dir` | Offline base URL | `/var/opt/kubitus/downloads` |
| `kubitus_data_dir` | Directory where Kubitus stores data | `/var/opt/kubitus/config/{{ kubitus_inventory_name }}` |
| `kubitus_credentials_dir` | Directory where Kubitus stores credential (on localhost) | `"{{ ansible_inventory_sources \| last \| dirname }}"` |
| `tenants` | [Tenants](#tenants) | `[]` |
| `kubitus_tenant` | Tenant applied to namespaces created by Kubitus-Installer | `'1'` |
| `workload_clusters` | List of workload cluster names | `[]` |

Kubeadm Patches variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `kube_apiserver_memory_request` | Memory request for kube-apiserver | `null` |
| `kube_apiserver_memory_limit` | Memory limit for kube-apiserver | `null` |
| `etcd_memory_request` | Memory request for etcd | `null` |
| `etcd_memory_limit` | Memory limit for etcd | `null` |
| `kube_controller_manager_memory_request` | Memory request for kube-controller | `null` |
| `kube_controller_manager_memory_limit` | Memory limit for kube-controller | `null` |
| `kube_scheduler_memory_request` | Memory request for kube-scheduler | `null` |
| `kube_scheduler_memory_limit` | Memory limit for kube-scheduler | `null` |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `ansible_python_interpreter` | See [Interpreter Discovery](https://docs.ansible.com/ansible/latest/reference_appendices/interpreter_discovery.html) | `/usr/bin/python3` |
| `kubitus_inventory_name` | Kubitus inventory name. Allows to use the same Kubespray node for several clusters. | `"{{ ansible_inventory_sources \| last \| dirname \| basename }}"` |
| `proxy_env` |  | :page_with_curl: |
| `netpol_default_fqdn2ips` |  | :page_with_curl: |
