# Releasing

Install and run `semantic-release`:

```shell
npm install semantic-release \
            semantic-release/changelog \
            semantic-release/git \
            conventional-changelog-conventionalcommits \
            commitlint \
            @commitlint/config-conventional
npx semantic-release --no-ci
```
