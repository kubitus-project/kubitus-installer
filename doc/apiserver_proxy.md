# APIserver proxy

## Scope

The `apiserver_proxy` role creates service and ingress to access a remote APIserver.

## How it works

The ingress authenticate using TLS certificates. You can set the CA with:

```yaml
apiserver_proxy_sealed_secret_auth_tls_data_ca_crt_value:
  -----BEGIN CERTIFICATE-----
  ...
  -----END CERTIFICATE-----
```

> Tip: you can create a PKI using vault.

The DN is passed in the `ssl-client-subject-dn` header.

You'll need this config in the workload cluster:

```yaml
kubespray_extravars:
  kube_kubeadm_apiserver_extra_args:
    requestheader-username-headers: X-Remote-User,ssl-client-subject-dn

# RBAC
global_impersonate_cluster_admin_users:
- CN=mycertsubject  # replace with the DN of the certificate
```

You'll also need to copy certificates from the first control-plane of the workload cluster
to the inventory. Example with `cluster-mgmt` to `cluster-workload1`:

- `/etc/kubernetes/ssl/front-proxy-ca.crt` to `../inventories/cluster-mgmt/apiserver_proxy_instance_cluster_workload1_sealed_secret_proxy_tls_data_ca_crt.creds`
- `/etc/kubernetes/ssl/front-proxy-client.crt` to `../inventories/cluster-mgmt/apiserver_proxy_instance_cluster_workload1_sealed_secret_proxy_tls_data_tls_crt.creds`
- `/etc/kubernetes/ssl/front-proxy-client.key` to `../inventories/cluster-mgmt/apiserver_proxy_instance_cluster_workload1_sealed_secret_proxy_tls_data_tls_key.creds`

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/argocd.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `apiserver_proxy_enabled` | Enable APIserver proxy | `false` |
| `apiserver_proxy_instances` | Instances | `"{{ workload_clusters }}"` |
| `apiserver_proxy_description` | Description | `APIserver proxy` |
| `apiserver_proxy_ingress_class` | Ingress class. Only `nginx` or `nginx-*` is supported | `"{{ kubitus_ingress_class if kubitus_ingress_class.startswith('nginx') else 'nginx' }}"` |
| `apiserver_proxy_sealed_secrets` | Sealed secrets | `[proxy-tls, auth-tls]` |
| `apiserver_proxy_sealed_secret_proxy_tls_type` | `proxy-tls` secret type | `kubernetes.io/tls` |
| `apiserver_proxy_sealed_secret_proxy_tls_items` | `proxy-tls` secret items | `[ca.crt, tls.crt, tls.key]` |
| `apiserver_proxy_sealed_secret_auth_tls_items` | `auth-tls` secret items | `[ca.crt]` |

Other public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `apiserver_proxy_service_external_name` | APIserver remote fqdn | `"apiserver.{{ gitops_instance.removeprefix('cluster-') }}.{{ top_level_domain.removeprefix(kubitus_inventory_name.removeprefix('cluster-')+'.') }}"` |
| `apiserver_proxy_fqdn` | APIserver published fqdn | `"apiserver{{ gitops_instance_variables.suffix }}.{{ top_level_domain }}"` |
| `apiserver_proxy_ingress_certificate_issuer` | APIserver ingress certificate issuer. See [`default_ingress_certificate_issuer`](kubitus_defaults.md#variables) for possible values | `"{{ default_ingress_certificate_issuer }}"` |
