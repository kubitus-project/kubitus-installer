# Metal3

## Scope

The `metal3` role installs [baremetal-operator](https://book.metal3.io/bmo/introduction)
and ironic.

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/metal3.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `metal3_enabled` | Enable metal3 | `false` |
| `metal3_namespace_pod_security` | Pod Security of the instance namespace | `privileged` |
| `metal3_helm_dependencies` | Helm dependencies | :page_with_curl: |
| `metal3_extra_namespace_resource_whitelist` | Extra namespace resource whitelist | :page_with_curl: |

Public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `metal3_ironic_ipa_downloader_ca_certificates` |  | `"{% if offline_enabled %}{{ local_ca_certificates }}{% else %}{{ None }}{% endif %}"` |
| `metal3_provisioning_interface` | Ironic provisioning interface | `eth0` |

Artefact variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `metal3_chart_version` |  | :page_with_curl: |
| `metal3_ironic_python_agent_branch` |  | `master` |
| `metal3_ironic_python_agent_flavor` |  | `centos9` |
| `metal3_image` |  | :page_with_curl: |
| `metal3_kube_rbac_proxy_image` |  | :page_with_curl: |
| `metal3_ironic_image` |  | :page_with_curl: |
| `metal3_mariadb_image` |  | :page_with_curl: |
| `metal3_keepalived_image` |  | :page_with_curl: |
| `metal3_ironic_ipa_downloader_image` |  | :page_with_curl: |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `metal3_projects` |  | :page_with_curl: |
