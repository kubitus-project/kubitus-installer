# Trivy Operator

## Scope

The `trivy_operator` role installs [Trivy Operator](https://aquasecurity.github.io/trivy-operator/latest/), a security toolkit.

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/trivy_operator.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `trivy_operator_enabled` | Enable Trivy Operator | `false` |
| `trivy_operator_description` | Description | `Trivy Operator` |
| `trivy_operator_namespace` | Namespace | `trivy-system` |
| `trivy_operator_namespace_pod_security` | Pod Security of the instance namespace | `privileged` |
| `trivy_operator_helm_dependencies` | Helm dependencies | :page_with_curl: |
| `trivy_operator_extra_cluster_resource_whitelist` | Extra cluster resource whitelist | :page_with_curl: |

Other public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `trivy_operator_target_namespaces` | Target namespaces list (no value to select all namespaces) | `null` |
| `trivy_operator_exclude_namespaces` | Exclude namespaces list | `null` |
| `trivy_operator_target_workloads` | Target Workload list | `[pod,replicaset,replicationcontroller,statefulset,daemonset,cronjob,job]` |
| `trivy_operator_scan_jobs_concurrent_limit` | Maximum number of scan jobs created by the operator | `10` |
| `trivy_operator_metrics_vuln_id_enabled` | Enable metrics about CVE vulns id | `false` |
| `trivy_operator_ignore_unfixed` | Show only fixed vulnerabilities | `false` |
| `trivy_operator_memory_request` | | `256Mi` |
| `trivy_operator_cpu_request` | | `1m` |
| `trivy_operator_memory_limit` | | `800Mi` |
| `trivy_operator_cpu_limit` | | `1000m` |
| `trivy_operator_trivy_scan_memory_request` | | `100M` |
| `trivy_operator_trivy_scan_cpu_request` | | `100m` |
| `trivy_operator_trivy_scan_memory_limit` | | `500M` |
| `trivy_operator_trivy_scan_cpu_limit` | | `500m` |
| `trivy_operator_chart_version` | | :page_with_curl: |
| `trivy_operator_image` | | :page_with_curl: |
| `trivy_operator_trivy_image` | | :page_with_curl: |
| `trivy_operator_db_image` | | :page_with_curl: |
| `trivy_operator_java_db_image` | | :page_with_curl: |
| `trivy_operator_node_collector_image` | | :page_with_curl: |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `trivy_operator_projects` |  | :page_with_curl: |
