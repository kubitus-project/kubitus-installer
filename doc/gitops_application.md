# Create a role for a GitOps application

[[_TOC_]]

## Naming

First, pick a `gitops_name` which should be a valid namespace name (i.e. `[a-z0-9]([-a-z0-9]*[a-z0-9])?`). We'll take `gitops_name: external-dns` here.

The derivated `role` has dashes replaced by underscores (i.e. `external_dns`).

## Main task

Create the main task in `roles/<role>/tasks/main.yml` (i.e. `roles/external_dns/tasks/main.yml`). For example:

```yaml
- name: ExternalDNS
  tags:
  - external-dns
  - external-dns_main
  when:
  - external_dns_enabled
  block:
  - name: ExternalDNS | Sync Helm chart and images
    include_tasks: "../../gitlabracadabra/tasks/process_actions.yml"
    vars:
      gitlab_projects: "{{ external_dns_projects }}"
    when: offline_enabled


  - name: ExternalDNS | GitOps
    import_role:
      name: gitops
      tasks_from: application
    vars:
      gitops_name: external-dns


- name: ExternalDNS - delete
  tags:
  - external-dns
  - external-dns_main
  when:
  - not external_dns_enabled
  block:
  - name: ExternalDNS | GitOps
    import_role:
      name: gitops
      tasks_from: delete_application
    vars:
      application_name: external-dns
```

## Chart values

Create the Jinja2 chart values template in `roles/<role>/templates/values.yaml.j2` (i.e. `roles/external_dns/templates/values.yaml.j2`). For example:

```yaml
external-dns:
  image:
    {{ gitops_instance_variables.image | imagesplit('repository,tag') | to_nice_yaml(indent=2) | indent(width=4) }}


  serviceMonitor:
    enabled: {{ prometheus_stack_enabled | to_json }}

    additionalLabels:
      release: prometheus-stack
```

Notes:

- `gitops_instance_variables.` is used to allow multiple instances
- `imagesplit` filter is used to extract image repository and tag. See [`filter_plugins/imagesplit.py`](filter_plugins/imagesplit.py)
- `ServiceMonitor` is enabled when `prometheus-stack` is enabled, and
  the `release: prometheus-stack` is set

## Default variables

Create a default variables file in `roles/kubitus_defaults/defaults/main/<role>.yml` (i.e. `roles/kubitus_defaults/defaults/main/external_dns.yml`). For example:

```yaml
# GitOps
external_dns_enabled: false
external_dns_description: ExternalDNS
external_dns_helm_dependencies:
- name: external-dns
  version: "{{ external_dns_chart_version }}"
  repository: "{% if offline_enabled
    %}https://{{ gitlab_fqdn }}/api/v4/projects/external-packages%2Fexternal-dns/packages/helm/stable/{%
    else
    %}https://kubernetes-sigs.github.io/external-dns{% endif %}"


external_dns_chart_version: 1.2.0
external_dns_image: registry.k8s.io/external-dns/external-dns:0.13.5


external_dns_projects:
  external-packages/external-dns:
    extends: .packages
    package_mirrors:
    - helm:
        repo_url: https://kubernetes-sigs.github.io/external-dns
        package_name: external-dns
        versions:
        - "{{ external_dns_chart_version }}"

  external-registries/registry.k8s.io:
    extends: .registry
    image_mirrors:
    - from: "{{ external_dns_image }}"
```

Notes:

- See [gitops](gitops.md) for all available GitOps variables.

## Apps

Add the app in `apps.yml`. For example:

```diff
diff --git a/apps.yml b/apps.yml
+- name: ExternalDNS
+  hosts: localhost
+  gather_facts: false
+  any_errors_fatal: "{{ any_errors_fatal | default(true) }}"
+  roles:
+  - role: kubitus_defaults
+  - role: external_dns
+
```

Note: Ensure the app is properly placed (i.e. after its dependencies).

## Documentation

Each variable should be documented in `doc/<role>.md` (i.e. `doc/external_dns.md`).

This can be tested with:

```console
$ ci/test-consistency
[...]
WARNING  Variable external_dns_enabled not documented in doc file: doc/external_dns.md
WARNING  Variable external_dns_description not documented in doc file: doc/external_dns.md
[...]
```

Also ensure the role is mentioned in top `README.md`:

```diff
diff --git a/README.md b/README.md
+| **ExternalDNS** (DNS) | &#9744; | [:book:](doc/external_dns.md) | *Helm@ArgoCD* |
```

Note: Ensure same position as in `apps.yml`.

## Sample inventory

Add most important variables in `inventories/sample/group_vars/all/all.yml`. For example:

```diff
+# external_dns_enabled: false
```

## Renovate

Ensure renovate is properly configured. For example:

```diff
diff --git a/renovate.json b/renovate.json
index 0b972d33..f3d753da 100644
--- a/renovate.json
+++ b/renovate.json
@@ -94,6 +94,12 @@
       ],
       "groupName": "docker"
     },
+    {
+      "matchFiles": [
+        "roles/kubitus_defaults/defaults/main/external_dns.yml"
+      ],
+      "groupName": "external_dns"
+    },
     {
       "matchFiles": [
         "roles/kubitus_defaults/defaults/main/gatekeeper.yml"
@@ -552,6 +558,17 @@
     },


+    {
+      "fileMatch": ["^roles/kubitus_defaults/defaults/main/external_dns\\.yml$"],
+      "matchStrings": [
+        "external_dns_chart_version:\\s*(?<currentValue>.*)\\s"
+      ],
+      "datasourceTemplate": "helm",
+      "registryUrlTemplate": "https://kubernetes-sigs.github.io/external-dns/",
+      "depNameTemplate": "external-dns"
+    },
+
+
     {
       "fileMatch": ["^roles/kubitus_defaults/defaults/main/gatekeeper\\.yml$"],
       "matchStrings": [
```

Note: This is also tested with `ci/test-consistency`.

## Network policies

TBD

## Resource limits and requests

TBD

## Monitoring

- PrometheusRule
- Grafana dashboards

TBD