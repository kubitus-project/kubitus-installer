# Upgrade

## Scope

The `upgrade` role :

- updates `apt` cache on all nodes
- drain node if applicable (in groups `kube_control_plane` or `kube_node`) and needed (upgrade needed or `upgrade_reboot == 'always'`)
- runs `aptitude dist-upgrade` if needed
- reboots if needed
- uncordon node if applicable and needed

Example usage:

```bash
ansible-playbook \
    --ask-pass \
    --ask-become-pass \
    external_components.yml \
    --tags upgrade \
    -e upgrade_enabled=true \
    -e upgrade_confirm_before_upgrade=true \
    -e upgrade_confirm_before_uncordon=true
```

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/prerequisites.yml).

Public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `upgrade_enabled` | Enable upgrade | `false` |
| `upgrade_confirm_before_upgrade` | Confirm before upgrade | `false` |
| `upgrade_reboot` | Reboot after upgrade. Possible values: `never`, `on-dist-upgrade`, `always` | `on-dist-upgrade` |
| `upgrade_confirm_before_uncordon` | Confirm before uncordon | `false` |
