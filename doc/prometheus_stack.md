# Prometheus stack

## Scope

The `prometheus_stack` role installs a complete Prometheus Stack, including Grafana.

## Accessing Grafana

The Grafana `"admin"` password is available from a secret:

```console
$ GRAFANA_ADMIN_PASSWORD="$(kubectl get secrets -n grafana grafana-admin -ojson | jq '.data["admin-password"]' -r | base64 -d)"
$ echo $GRAFANA_ADMIN_PASSWORD
AbCdEf
```

Or from the `kubitus-installer` node (`localhost`):

```console
$ GRAFANA_ADMIN_PASSWORD="$(cat inventories/$inventory/grafana_admin_password.creds)"
$ echo $GRAFANA_ADMIN_PASSWORD
AbCdEf
```

## Grafana OIDC

By default, only `"{{ top_level_domain }}"` is accepted as email domain,
you can change this in the inventory:

```yaml
grafana_generic_oauth_allowed_domains:
- example.org
- example.com
```

## Handling alerts

See [alerts](alerts.md).

## Namespace enforcement

Namespace label is enforced, except for specific `ServiceMonitor`s or `PrometheusRule`s.
Additional exceptions can be added with, for example:

```yaml
prometheus_stack_excluded_from_enforcement:
- group: monitoring.coreos.com
  resource: prometheusrules
  namespace: cluster-owner-prometheus-rules
  name: cluster-owner
```

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/prometheus_stack.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `prometheus_stack_enabled` | Enable Prometheus stack | `true` |
| `prometheus_stack_description` | Description | `Prometheus Stack` |
| `prometheus_stack_extra_namespaces` | Extra namespaces | `[grafana, kube-system, kube-state-metrics, prometheus-node-exporter]` |
| `prometheus_stack_helm_dependencies` | Helm dependencies | :page_with_curl: |
| `prometheus_stack_ignore_differences` | Ignore differences | :page_with_curl: |
| `prometheus_stack_server_side_apply` | Server-side apply | `true` |
| `prometheus_stack_extra_namespace_resource_whitelist` | Extra namespace resource whitelist | :page_with_curl: |
| `prometheus_stack_sealed_secrets` | Sealed Secrets | :page_with_curl: |
| `prometheus_stack_sealed_secret_grafana_admin_namespace` | `grafana-admin` secret namespace | `grafana` |
| `prometheus_stack_sealed_secret_grafana_admin_unencrypted_data` | `grafana-admin` secret unencrypted data | `{admin-user: admin}` |
| `prometheus_stack_sealed_secret_grafana_admin_items` | `grafana-admin` secret items | `[admin-password]` |
| `prometheus_stack_sealed_secret_grafana_admin_data_admin_password_value` | Grafana `admin-password` value | `"{{ gitops_instance_variables.grafana_admin_password }}"` |
| `prometheus_stack_sealed_secret_keycloak_client_secret_grafana_namespace` | `keycloak-client-secret-grafana` secret namespace | `grafana` |
| `prometheus_stack_sealed_secret_keycloak_client_secret_grafana_items` | `keycloak-client-secret-grafana` secret items | `[CLIENT_ID, CLIENT_SECRET]` |
| `prometheus_stack_sealed_secret_keycloak_client_secret_grafana_data_client_id_value` | Grafana `CLIENT_ID` value | `"{{ gitops_instance_variables.grafana_oidc_client_id }}"` |
| `prometheus_stack_sealed_secret_keycloak_client_secret_grafana_data_client_secret_value` |  Grafana `CLIENT_SECRET` value `"{{ gitops_instance_variables.grafana_oidc_client_secret }}"` |
| `prometheus_stack_sealed_secret_grafana_loki_datasources_namespace` | `grafana-loki-datasources` secret namespace | `grafana` |
| `prometheus_stack_sealed_secret_grafana_loki_datasources_labels` | `grafana-loki-datasources` secret labels | `{grafana_datasource: "1"}` |
| `prometheus_stack_sealed_secret_grafana_loki_datasources_items` | `grafana-loki-datasources` secret items | `[grafana-loki-datasources.yaml]` |
| `prometheus_stack_sealed_secret_grafana_loki_datasources_data_grafana_loki_datasources_yaml_value` | `grafana-loki-datasources.yaml` content | :page_with_curl: |
| `prometheus_stack_sealed_secret_grafana_prometheus_datasources_namespace` | `grafana-prometheus-datasources` secret namespace | `grafana` |
| `prometheus_stack_sealed_secret_grafana_prometheus_datasources_labels` | `grafana-prometheus-datasources` secret labels | `{grafana_datasource: "1"}` |
| `prometheus_stack_sealed_secret_grafana_prometheus_datasources_items` | `grafana-prometheus-datasources` secret items | `[grafana-prometheus-datasources.yaml]` |
| `prometheus_stack_sealed_secret_grafana_prometheus_datasources_data_grafana_prometheus_datasources_yaml_value` | `grafana-prometheus-datasources.yaml` content | :page_with_curl: |

Operator variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `prometheus_stack_create_aggregate_cluster_roles` | Allow namespace admins to create Prometheus CRDs (`ServiceMonitors`, `PrometheusRules`, ...) | `true` |
| `prometheus_stack_excluded_from_enforcement` | Additional resources where namespace is not enforced | `[]` |
| `prometheus_stack_operator_admissionwbebhooks_patch_memory_request` | | `100Mi` |
| `prometheus_stack_operator_admissionwbebhooks_patch_cpu_request` | | `10m` |
| `prometheus_stack_operator_admissionwbebhooks_patch_memory_limit` | | `100Mi` |
| `prometheus_stack_operator_admissionwbebhooks_patch_cpu_limit` | | `null` |
| `prometheus_stack_operator_memory_request` | | `100Mi` |
| `prometheus_stack_operator_cpu_request` | | `1m` |
| `prometheus_stack_operator_memory_limit` | | `1Gi` |
| `prometheus_stack_operator_cpu_limit` | | `null` |
| `prometheus_stack_chart_version` | | :page_with_curl: |
| `prometheus_stack_operator_image` | | :page_with_curl: |
| `prometheus_stack_kube_webhook_certgen_image` | | :page_with_curl: |
| `prometheus_stack_config_reloader_image` | | :page_with_curl: |


Prometheus variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `prometheus_stack_prometheus_ingress_enabled` | Enable Prometheus Ingress | `false` |
| `prometheus_stack_prometheus_fqdn` | Prometheus DNS name | `"prometheus.{{ top_level_domain }}"` |
| `prometheus_stack_prometheus_ingress_certificate_issuer` | Prometheus ingress certificate issuer. See [`default_ingress_certificate_issuer`](kubitus_defaults.md#variables) for possible values | `"{{ default_ingress_certificate_issuer }}"` |
| `prometheus_stack_prometheus_persistence_size` | | `50Gi` |
| `prometheus_stack_prometheus_memory_request` | | `2Gi` |
| `prometheus_stack_prometheus_cpu_request` | | `300m` |
| `prometheus_stack_prometheus_memory_limit` | | `12Gi` |
| `prometheus_stack_prometheus_cpu_limit` | | `null` |
| `prometheus_stack_prometheus_image` | | :page_with_curl: |
| `prometheus_stack_prometheus_retention` | | `30d` |

Alertmanager variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `prometheus_stack_alertmanager_ingress_enabled` | Enable Alertmanager Ingress | `false` |
| `prometheus_stack_alertmanager_fqdn` | Alertmanager DNS name | `"alertmanager.{{ top_level_domain }}"` |
| `prometheus_stack_alertmanager_ingress_certificate_issuer` | Alertmanager ingress certificate issuer. See [`default_ingress_certificate_issuer`](kubitus_defaults.md#variables) for possible values | `"{{ default_ingress_certificate_issuer }}"` |
| `prometheus_stack_alertmanager_config` | Alertmanager configuration. See `https://prometheus.io/docs/alerting/latest/configuration` for possible values | `{}` |
| `prometheus_stack_alertmanager_memory_request` | | `250Mi` |
| `prometheus_stack_alertmanager_cpu_request` | | `200m` |
| `prometheus_stack_alertmanager_memory_limit` | | `250Mi` |
| `prometheus_stack_alertmanager_cpu_limit` | | `null` |
| `prometheus_stack_alertmanager_image` | | :page_with_curl: |

Grafana variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `prometheus_stack_grafana_enabled` | Enable Grafana (when `prometheus_stack_enabled=true`) | `true` |
| `prometheus_stack_grafana_fqdn` | Grafana DNS name | `"grafana.{{ top_level_domain }}"` |
| `prometheus_stack_grafana_ingress_certificate_issuer` | Grafana ingress certificate issuer. See [`default_ingress_certificate_issuer`](kubitus_defaults.md#variables) for possible values | `"{{ default_ingress_certificate_issuer }}"` |
| `prometheus_stack_grafana_admin_password` | Grafana admin password | `"{{ lookup('password', kubitus_credentials_dir + '/grafana_admin_password.creds length=32 chars=ascii_letters,digits') }}"` |
| `prometheus_stack_grafana_oidc_enabled` | Enable Grafana OIDC see [Grafana OIDC](#grafana-oidc) | `"{{ keycloak_enabled }}"` |
| `prometheus_stack_grafana_oidc_url` | Base URL for OIDC | `https://{{ keycloak_fqdn }}/realms/default` |
| `prometheus_stack_grafana_oidc_auth_url` | OIDC auth URL | `"{{ grafana_oidc_url }}/protocol/openid-connect/auth"` |
| `prometheus_stack_grafana_oidc_token_url` | OIDC token URL | `"{{ grafana_oidc_url }}/protocol/openid-connect/token"` |
| `prometheus_stack_grafana_oidc_api_url` | OIDC API URL | `"{{ grafana_oidc_url }}/protocol/openid-connect/userinfo"` |
| `prometheus_stack_grafana_oidc_client_id` | OIDC client ID | `grafana` |
| `prometheus_stack_grafana_oidc_client_secret` | OIDC client secret | `"{{ lookup('password', kubitus_credentials_dir + '/grafana_oidc_client_secret.creds length=32 chars=ascii_letters,digits') }}"` |
| `prometheus_stack_grafana_auth_disable_login_form` | [Disable login form](https://grafana.com/docs/grafana/latest/auth/grafana/#disable-login-form) | `false` |
| `prometheus_stack_grafana_auth_oauth_auto_login` | [Automatic OAuth login](https://grafana.com/docs/grafana/latest/auth/grafana/#automatic-oauth-login) | `false` |
| `prometheus_stack_grafana_auth_oauth_allow_insecure_email_lookup` | [Enable email lookup](https://grafana.com/docs/grafana/latest/setup-grafana/configure-security/configure-authentication/#enable-email-lookup) | `true` |
| `prometheus_stack_grafana_auth_grafana_admin_to_org_admins` | Ensure GrafanaAdmin users are admin in all Orgs. NB: Requires [grafana pull request #77683](https://github.com/grafana/grafana/pull/78560)  | `false` |
| `prometheus_stack_grafana_generic_oauth_allowed_domains` | Email domains allowed to authenticate. See [generic-oauth documentation](https://grafana.com/docs/grafana/latest/auth/generic-oauth/) | `["{{ top_level_domain }}"]` |
| `prometheus_stack_grafana_generic_oauth_allow_sign_up` | Allow users to sign-up with OAuth. See [generic-oauth documentation](https://grafana.com/docs/grafana/latest/auth/generic-oauth/) | `true` |
| `prometheus_stack_grafana_generic_oauth_role_attribute_path` | Grafana role attribute path. NB: `role_attribute_strict` and `allow_assign_grafana_admin` are both set to `true` | `"email && 'Viewer'"` |
| `prometheus_stack_grafana_generic_oauth_org_roles_attribute_path` | Grafana org to role attribute path. NB: Requires old version of  [grafana pull request #77683](https://github.com/grafana/grafana/pull/77683) | `~` |
| `prometheus_stack_grafana_generic_oauth_org_attribute_path` | Grafana org to role attribute path. NB: Requires[grafana pull request #77683](https://github.com/grafana/grafana/pull/77683) | `~` |
| `prometheus_stack_grafana_generic_oauth_org_mapping` | List of `Value:OrgIdOrName:Role` mappings. NB: Requires [grafana pull request #77683](https://github.com/grafana/grafana/pull/77683) | `~` |
| `prometheus_stack_grafana_smtp_host` | Grafana's SMTP host | `null` |
| `prometheus_stack_grafana_alerting` | Configure grafana altering. See [here](https://github.com/helm-charts/blob/main/charts/grafana/values.yaml#L551) | `{}` |
| `prometheus_stack_grafana_provision_orgs` | Provision Orgs for tenants. NB: Requires [grafana pull request #78253](https://github.com/grafana/grafana/pull/78253) | `false` |
| `prometheus_stack_grafana_datasource_orgnames` | Use tenant name as `orgName` in datasources provisioning. NB: Requires [grafana pull request #77937](https://github.com/grafana/grafana/pull/77937) | `false` |
| `prometheus_stack_grafana_default_dashboards_timezone` | Configure grafana default dashbord timezone | `Europe/Paris` |
| `prometheus_stack_grafana_persistence_size` | | `10Gi` |
| `prometheus_stack_grafana_downloaddashboards_memory_request` | | `128Mi` |
| `prometheus_stack_grafana_downloaddashboards_cpu_request` | | `1m` |
| `prometheus_stack_grafana_downloaddashboards_memory_limit` | | `256Mi` |
| `prometheus_stack_grafana_downloaddashboards_cpu_limit` | | `null` |
| `prometheus_stack_grafana_memory_request` | | `128Mi` |
| `prometheus_stack_grafana_cpu_request` | | `3m` |
| `prometheus_stack_grafana_memory_limit` | | `1024Mi` |
| `prometheus_stack_grafana_cpu_limit` | | `null` |
| `prometheus_stack_grafana_sidecar_memory_request` | | `128Mi` |
| `prometheus_stack_grafana_sidecar_cpu_request` | | `1m` |
| `prometheus_stack_grafana_sidecar_memory_limit` | | `256Mi` |
| `prometheus_stack_grafana_sidecar_cpu_limit` | | `null` |
| `prometheus_stack_grafana_image` | | :page_with_curl: |
| `prometheus_stack_grafana_k8s_sidecar_image` | | :page_with_curl: |


kube-state-metrics variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `prometheus_stack_kube_state_metrics_memory_request` | | `50Mi` |
| `prometheus_stack_kube_state_metrics_cpu_request` | | `1m` |
| `prometheus_stack_kube_state_metrics_memory_limit` | | `200Mi` |
| `prometheus_stack_kube_state_metrics_cpu_limit` | | `null` |
| `prometheus_stack_kube_state_metrics_image` | | :page_with_curl: |

prometheus-node-exporter variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `prometheus_stack_prometheus_node_exporter_memory_request` | | `100Mi` |
| `prometheus_stack_prometheus_node_exporter_cpu_request` | | `4m` |
| `prometheus_stack_prometheus_node_exporter_memory_limit` | | `200Mi` |
| `prometheus_stack_prometheus_node_exporter_cpu_limit` | | `null` |
| `prometheus_stack_prometheus_node_exporter_image` | | :page_with_curl: |

scrapping variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `prometheus_stack_gitlab_external_scrapeconfig_targets` | Gitlab targets to scrape metrics from. Example `[10.20.30.40:9090]` | `[]` |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `prometheus_stack_projects` |  | :page_with_curl: |
