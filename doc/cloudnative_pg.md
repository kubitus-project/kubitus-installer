# CloudNativePG

## Scope

The `cloudnative_pg` role installs [CloudNativePG](https://cloudnative-pg.io/), a PostgreSQL operator.

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/cloudnative_pg.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `cloudnative_pg_enabled` | Enable CloudNativePG | `false` |
| `cloudnative_pg_description` | Description | `CloudNativePG` |
| `cloudnative_pg_namespace` | Namespace | `cnpg-system` |
| `cloudnative_pg_helm_dependencies` | Helm dependencies | :page_with_curl: |
| `cloudnative_pg_server_side_apply` | Server-side apply | `true` |
| `cloudnative_pg_ignore_differences` | Ignore differences | :page_with_curl: |

Other public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `cloudnative_pg_memory_request` | | `60Mi` |
| `cloudnative_pg_cpu_request` | | `null` |
| `cloudnative_pg_memory_limit` | | `100Mi` |
| `cloudnative_pg_cpu_limit` | | `100m` |
| `cloudnative_pg_chart_version` | | :page_with_curl: |
| `cloudnative_pg_image` | | :page_with_curl: |
| `cloudnative_pg_postgresql_image` | | :page_with_curl: |
| `cloudnative_pg_version` | | :page_with_curl: |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `cloudnative_pg_short_version` |  | :page_with_curl: |
| `cloudnative_pg_projects` |  | :page_with_curl: |
| `cloudnative_pg_downloads` |  | :page_with_curl: |
