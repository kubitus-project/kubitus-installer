# OpenTelemetry Collector

## Scope

The `opentelemetry_collector` role installs [OpenTelemetry Collector](https://github.com/open-telemetry/opentelemetry-collector),
and create OpenTelemetry collectors.

## Logs

Currently, the following logs are received :

- `journald` (in the `daemonset` instance) : system logs
- `filelog` (in the `daemonset` instance) : Pod logs
- `k8sobjects` (in the `deployment` instance) : Kubernetes events. Example [below](#kubernetes-events).

### Kubernetes Events

The following attributes are extracted :

```yaml
{
  "body": {
    "object": {
      "apiVersion": "events.k8s.io/v1",
      "deprecatedCount": 1,
      "deprecatedFirstTimestamp": "2023-10-10T14:32:01Z",
      "deprecatedLastTimestamp": "2023-10-10T14:32:01Z",
      "deprecatedSource": {
        "component": "job-controller"
      },
      "eventTime": null,
      "kind": "Event",
      "metadata": {
        "creationTimestamp": "2023-10-10T14:32:01Z",
        "managedFields": [
          {
            "apiVersion": "v1",
            "fieldsType": "FieldsV1",
            "fieldsV1": {
              "f:count": {},
              "f:firstTimestamp": {},
              "f:involvedObject": {},
              "f:lastTimestamp": {},
              "f:message": {},
              "f:reason": {},
              "f:source": {
                "f:component": {}
              },
              "f:type": {}
            },
            "manager": "kube-controller-manager",
            "operation": "Update",
            "time": "2023-10-10T14:32:01Z"
          }
        ],
        "name": "scan-vulnerabilityreport-c74987f7d.178cc58200608dab",
        "namespace": "trivy-system",
        "resourceVersion": "56428294",
        "uid": "0edb8965-0d84-494e-944c-44599a463421"
      },
      "note": "Job completed",                        # ==> body
      "reason": "Completed",                          # ==> attributes["k8s.event.reason"]
      "regarding": {
        "apiVersion": "batch/v1",
        "kind": "Job",
        "name": "scan-vulnerabilityreport-c74987f7d", # ==> resource.attributes["k8s.job.name"]
        "namespace": "trivy-system",                  # ==> resource.attributes["k8s.namespace.name"]
        "resourceVersion": "56428290",
        "uid": "16268596-198c-4a89-b481-5e3421061e90"
      },
      "type": "Normal"                                # ==> attributes["k8s.event.type"] + severity_number
    },
    "type": "ADDED"
  }
}
```

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/opentelemetry_collector.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `opentelemetry_collector_enabled` | Enable OpenTelemetry Collector | `false` |
| `opentelemetry_collector_instances` | Instances | `[daemonset, deployment]` |
| `opentelemetry_collector_instance_daemonset_namespace_pod_security` | Namespace Pod Security, for the `daemonset` instance | `privileged` |
| `opentelemetry_collector_description` | Description | `OpenTelemetry Collector` |
| `opentelemetry_collector_helm_dependencies` | Helm dependencies | :page_with_curl: |

Mode and snippets variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `opentelemetry_collector_mode` | Mode (valid values are `daemonset`, `deployment` and `statefulset`) | `~` |
| `opentelemetry_collector_extra_filters` | Extra filters | `[]` |
| `opentelemetry_collector_connectors` | Add connectors | `[]` |
| `opentelemetry_collector_extra_exporters` | Extra exporters | `[]` |
| `opentelemetry_collector_extra_pipelines` | Extra pipelines | `[]` |
| `opentelemetry_collector_pipeline_logs_extra_exporters` | Extra exporters into logs pipelines | `[]` |
| `opentelemetry_collector_pipeline_logs2_extra_exporters` | Extra exporters into logs/2 pipelines | `[]` |
| `opentelemetry_collector_instance_daemonset_mode` | Mode of the `daemonset` instance | `daemonset` |
| `opentelemetry_collector_instance_deployment_mode` | Mode of the `deployment` instance | `deployment` |
| `opentelemetry_collector_logs_collection_enabled` | Adds the filelog receiver to the logs pipeline | `false` |
| `opentelemetry_collector_instance_daemonset_logs_collection_enabled` | Adds the filelog receiver to the logs pipeline, for the `daemonset` instance | `true` |
| `opentelemetry_collector_journald_enabled` | Adds the journald receiver to the logs pipeline | `false` |
| `opentelemetry_collector_instance_daemonset_journald_enabled` | Adds the journald receiver to the logs pipeline, for the `daemonset` instance | `"{{ not fluent_bit_enabled }}"` |
| `opentelemetry_collector_host_metrics_enabled` | Collect host metrics | `false` |
| `opentelemetry_collector_instance_daemonset_host_metrics_enabled` | Collect host metrics, for the `daemonset` instance | `false` |
| `opentelemetry_collector_kubernetes_attributes_enabled` | Adds Kubernetes metadata | `false` |
| `opentelemetry_collector_instance_daemonset_kubernetes_attributes_enabled` | Adds Kubernetes metadata, for the `daemonset` instance | `true` |
| `opentelemetry_collector_instance_deployment_kubernetes_attributes_enabled` | Adds Kubernetes metadata, for the `deployment` instance | `true` |
| `opentelemetry_collector_kubelet_metrics_enabled` | Collect node, pod and containers metrics | `false` |
| `opentelemetry_collector_instance_daemonset_kubelet_metrics_enabled` | Collect node, pod and containers metrics, for the `daemonset` instance | `true` |
| `opentelemetry_collector_kubernetes_events_enabled` | Collect Kubernetes events | `false` |
| `opentelemetry_collector_instance_deployment_kubernetes_events_enabled` | Collect Kubernetes events, for the `deployment` instance | `true` |
| `opentelemetry_collector_cluster_metrics_enabled` | Collect cluster-level metrics | `false` |
| `opentelemetry_collector_instance_deployment_cluster_metrics_enabled` | Collect cluster-level metrics, for the `deployment` instance | `true` |
| `opentelemetry_collector_ingress_nginx_namespace_is_tenant_regex` | If set, logs from ingress-nginx having namespace matching this regex will have tenant set to it. Workaround [opentelemetry-collector-contrib#36670](https://github.com/open-telemetry/opentelemetry-collector-contrib/issues/36670) | `null` |
| `opentelemetry_collector_minio_tenant_bucket_is_tenant_regex` | If set, logs from minio-tenant having bucket matching this regex will have tenant set to it. | `null` |

Resources variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `opentelemetry_collector_memory_request` | | `200Mi` |
| `opentelemetry_collector_instance_daemonset_memory_request` | | `200Mi` |
| `opentelemetry_collector_cpu_request` | | `3m` |
| `opentelemetry_collector_instance_daemonset_cpu_request` | | `30m` |
| `opentelemetry_collector_memory_limit` | | `600Mi` |
| `opentelemetry_collector_instance_daemonset_memory_limit` | | `1Gi` |
| `opentelemetry_collector_cpu_limit` | | `null` |

Artifacts variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `opentelemetry_collector_chart_version` |  | :page_with_curl: |
| `opentelemetry_collector_image` |  | :page_with_curl: |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `opentelemetry_collector_projects` |  | :page_with_curl: |
