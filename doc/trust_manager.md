# trust-manager

## Scope

The `trust_manager` role installs [trust-manager](https://cert-manager.io/docs/trust/trust-manager/),
to manage trust bundles.

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/trust_manager.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `trust_manager_enabled` | Enable trust-manager. :warning: `cert-manager` should be installed | `false` |
| `trust_manager_description` | Description | `trust-manager` |
| `trust_manager_extra_namespaces` | Extra namespaces | `[cert-manager]` |
| `trust_manager_helm_dependencies` | Helm dependencies | :page_with_curl: |
| `trust_manager_extra_namespace_resource_whitelist` | Extra namespace resource whitelist | :page_with_curl: |

Other public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `trust_manager_chart_version` | | :page_with_curl: |
| `trust_manager_image` | | :page_with_curl: |
| `trust_manager_default_package_image` | | :page_with_curl: |
| `trust_manager_defaultpackage_cpu_request` | | `10m` |
| `trust_manager_defaultpackage_memory_request` | | `40Mi` |
| `trust_manager_defaultpackage_cpu_limit` | | `100m` |
| `trust_manager_defaultpackage_memory_limit` | | `64Mi` |
| `trust_manager_cpu_request` | | `10m` |
| `trust_manager_memory_request` | | `40Mi` |
| `trust_manager_cpu_limit` | | `100m` |
| `trust_manager_memory_limit` | | `64Mi` |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `trust_manager_projects` |  | :page_with_curl: |
