# Fluent Bit

## Scope

The `fluent_bit` role installs [Fluent Bit](https://github.com/fluent/fluent-bit),.
Fast and lightweight log processor and forwarder for Linix, OSX and BSD family operating systems.

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/fluent_bit.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `fluent_bit_enabled` | Enable Fluent Bit | `false` |
| `fluent_bit_description` | Description | `Fluent Bit` |
| `fluent_bit_namespace_pod_security` |  | :page_with_curl: |
| `fluent_bit_helm_dependencies` | Helm dependencies | :page_with_curl: |

Resources variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `fluent_bit_memory_request` | | `128Mi` |
| `fluent_bit_cpu_request` | | `100m` |
| `fluent_bit_memory_limit` | | `128Mi` |
| `fluent_bit_cpu_limit` | | `100m` |

Other public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `fluent_bit_chart_version` |  | :page_with_curl: |
| `fluent_bit_image` |  | :page_with_curl: |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `fluent_bit_projects` |  | :page_with_curl: |
