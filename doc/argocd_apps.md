# ArgoCD apps

## Scope

The `argocd_apps` role installs the [argocd-apps](https://github.com/argoproj/argo-helm/tree/main/charts/argocd-apps),
helm chart, to deploy:

- Argo CD applications
- Argo CD application sets
- Argo UI extensions
- Argo CD projects

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/argocd_apps.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `argocd_apps_enabled` | Enable ArgoCD apps | `"{{ argocd_apps_applications or argocd_apps_applicationsets or argocd_apps_extensions or argocd_apps_projects }}"` |
| `argocd_apps_namespace` | Namespace | `argocd` |
| `argocd_apps_create_namespace` | Create instance namespace | `false` |
| `argocd_apps_helm_dependencies` | Helm dependencies | :page_with_curl: |
| `argocd_apps_extra_namespace_resource_whitelist` | Extra namespace resource whitelist | :page_with_curl: |

Other public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `argocd_apps_applications` | Maps of Argo CD applications | `{}` |
| `argocd_apps_applicationsets` | Maps of Argo CD application sets | `{}` |
| `argocd_apps_extensions` | Maps of Argo UI extensions | `{}` |
| `argocd_apps_projects` | Maps of Argo CD projects | `{}` |
| `argocd_apps_chart_version` | | :page_with_curl: |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `argocd_apps_gitlabracadabra_projects` |  | :page_with_curl: |
