# GitLab Runner

## Scope

The `gitlab_runner` role installs the [GitLab Runner](https://docs.gitlab.com/runner/),
for CI.

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/gitlab_runner.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `gitlab_runner_enabled` | Enable GitLab Runner | `false` |
| `gitlab_runner_description` | Description | `GitLab Runner` |
| `gitlab_runner_helm_dependencies` | Helm dependencies | :page_with_curl: |

Other public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `gitlab_runner_gitlab_url` | URL of GitLab | `https://gitlab.example.org/` |
| `gitlab_runner_registration_token` | GitLab registration token. *DEPRECATED* | `null` |
| `gitlab_runner_token` | GitLab Runner token | `null` |
| `gitlab_runner_replicas` | How many runner pods to launch | `2` |
| `gitlab_runner_concurrent` | Maximum number of concurrent jobs | `10` |
| `gitlab_runner_check_interval` | Define in seconds how often to check GitLab for a new build | `30` |
| `gitlab_runner_tags` | Specify the tags associated with the runner. Comma separated list of tags | `null` |
| `gitlab_runner_default_image` | Default image | `debian:bookworm` |
| `gitlab_runner_certs` | Pass custom certificates to GitLab Runner | `{}` |
| `gitlab_runner_memory_limit` | | `4Gi` |
| `gitlab_runner_memory_limit_overwrite_max_allowed` | | `8Gi` |
| `gitlab_runner_helper_memory_limit` | | `100Mi` |
| `gitlab_runner_helper_memory_limit_overwrite_max_allowed` | | `100Mi` |
| `gitlab_runner_service_memory_limit` | | `1Gi` |
| `gitlab_runner_service_memory_limit_overwrite_max_allowed` | | `2Gi` |
| `gitlab_runner_{,helper_,service_}{memory,cpu,ephemeral_storage}_{limit,request}{,_overwrite_max_allowed}` | | `null` |
| `gitlab_runner_chart_version` | | :page_with_curl: |
| `gitlab_runner_image` | | :page_with_curl: |
| `gitlab_runner_helper_image` | | :page_with_curl: |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `gitlab_runner_requests_limits_settings` |  | :page_with_curl: |
| `gitlab_runner_settings` |  | ``"{{ gitlab_runner_requests_limits_settings }}"`` |
| `gitlab_runner_projects` |  | :page_with_curl: |
