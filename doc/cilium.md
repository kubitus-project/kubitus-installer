# Cilium

## Scope

The `cilium` role installs [Cilium](https://helm.cilium.io/),
an eBPF-based networking with integrated observability and security.

## Upgrading to Cilium without kube-proxy

To disable `kube-proxy`:

- Apply:

    ```shell
    ansible-playbook \
        --ask-pass \
        --ask-become-pass \
        apps.yml \
        --tags cilium
    ```

- Uninstall `kube-proxy` `DaemonSet` and `ConfigMap`:

    ```shell
    kubectl delete -n kube-system ds/kube-proxy cm/kube-proxy
    ```

- Upgrade and reboot nodes to ensure `ipvs` and `nft` rules are cleaned:

    ```shell
    ansible-playbook \
        --ask-pass \
        --ask-become-pass \
        external_components.yml \
        --tags upgrade \
        --extra-vars upgrade_enabled=true \
        --extra-vars upgrade_confirm_before_upgrade=true \
        --extra-vars upgrade_reboot=always \
        -l kube_node,kube_control_plane
    ```

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/cilium.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `cilium_enabled` | Enable Cilium | `"{{ not terraform_gke_enabled }}"` |
| `cilium_namespace` | Namespace | `kube-system` |
| `cilium_extra_namespaces` | Namespace | `[cilium-secrets]` |
| `cilium_helm_dependencies` | Helm dependencies | :page_with_curl: |
| `cilium_ignore_differences` | Ignore differences | :page_with_curl: |
| `cilium_server_side_apply` | Server-side apply | `true` |
| `cilium_server_side_diff` | Server-Side diff | `true` |
| `cilium_extra_cluster_resource_whitelist` | Extra cluster resource whitelist | :page_with_curl: |
| `cilium_extra_namespace_resource_whitelist` | Extra namespace resource whitelist | :page_with_curl: |

Other public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `cilium_force_helm_install` | Force Cilium installation with Helm | `false` |
| `cilium_kube_proxy_replacement` | kube-proxy replacement in Cilium BPF datapath. Valid options are `false` or `true` | `true` |
| `cilium_k8s_service_host` | Kubernetes service host. Extracted from `KUBECONFIG` if empty | `""` |
| `cilium_k8s_service_port` | Kubernetes service port. Extracted from `KUBECONFIG` if empty | `6443` |
| `cilium_bpf_masquerade_enabled` | Enable BPF masquerade. Requires `cilium_kube_proxy_replacement=true` | `"{{ cilium_kube_proxy_replacement }}"` |
| `cilium_ipam_mode` | [IPAM mode](https://docs.cilium.io/en/stable/concepts/networking/ipam/) | `kubernetes` |
| `cilium_egress_gateway_enabled` | Enable egress gateway. Requires `cilium_kube_proxy_replacement=true` and `cilium_bpf_masquerade_enabled=true` | `false` |
| `cilium_tunnel` | Encapsulation configuration for communication between nodes. Possible values : `disabled`, `vxlan`, `geneve` | `disabled` |
| `cilium_ipv4_native_routing_cidr` | Explicitly specify the IPv4 CIDR for native routing | `"{% if cilium_tunnel == 'disabled' %}{{ [kube_service_addresses, kube_pods_subnet] | ansible.utils.cidr_merge('span') }}{% else %}{{ None }}{% endif %}"` |
| `cilium_auto_direct_node_routes` | Enable installation of PodCIDR routes between worker nodes | `"{{ cilium_tunnel == 'disabled' }}"` |
| `cilium_loadbalancer_mode` | LoadBalancer mode. One of `snat`, `dsr`, `hybrid` | `"{% if cilium_kube_proxy_replacement not in [false, 'disabled'] and cilium_tunnel == 'disabled' %}dsr{% else %}snat{% endif %}"` |
| `cilium_policy_enforcement_mode` | Policy Enforcement Modes. One of `default`, `always`, `never` | `default` |
| `cilium_metrics` | Agent metrics that should be enabled or disabled from the default metric list. Example: `['+cilium_fqdn_active_names']` See [Cilium documentation](https://docs.cilium.io/en/stable/observability/metrics/) | `~` |
| `cilium_ingress_controller_enabled` | Enable Kubernetes ingress support | `false` |
| `cilium_ingress_controller_loadbalancer_mode` | Default ingress load balancer mode   | `shared` |
| `cilium_ingress_controller_service_type` | Service type for the shared LB service | `LoadBalancer` |
| `cilium_ingress_controller_service_load_balancer_ip` | Cilium ingress controller LoadBalancer IP | `null` |
| `cilium_hubble_relay_enabled` | Enable Hubble relay | `"{{ cilium_hubble_ui_enabled }}"` |
| `cilium_hubble_ui_enabled` | Enable Hubble UI | `false` |
| `cilium_hubble_export_dynamic_enabled` | Enable Hubble dynamic flow logs exporter | `false` |
| `cilium_hubble_export_dynamic_config` | | :page_with_curl: |
| `cilium_hostfirewall_enabled` | Enable the enforcement of host policies in the eBPF datapath | `false` |
| `cilium_ssh_ingress_access_cidr` | CIDR for allow SSH access | `null` |
| `cilium_extra_host_network_policies` | Extra host network policy | :page_with_curl: |
| `cilium_hubble_metrics` | Hubble metrics | `[dns:query;ignoreAAAA, drop, tcp, flow, icmp, httpV2]` |
| `cilium_memory_request` | | `200Mi` |
| `cilium_cpu_request` | | `7m` |
| `cilium_memory_limit` | | `500Mi` |
| `cilium_cpu_limit` | | `null` |
| `cilium_hubble_relay_memory_request` | | `50Mi` |
| `cilium_hubble_relay_cpu_request` | | `1m` |
| `cilium_hubble_relay_memory_limit` | | `100Mi` |
| `cilium_hubble_relay_cpu_limit` | | `null` |
| `cilium_envoy_memory_request` | | `50Mi` |
| `cilium_envoy_cpu_request` | | `100m` |
| `cilium_envoy_memory_limit` | | `4Gi` |
| `cilium_envoy_cpu_limit` | | `4000m` |
| `cilium_hubble_ui_backend_memory_request` | | `50Mi` |
| `cilium_hubble_ui_backend_cpu_request` | | `3m` |
| `cilium_hubble_ui_backend_memory_limit` | | `1Gi` |
| `cilium_hubble_ui_backend_cpu_limit` | | `null` |
| `cilium_hubble_ui_frontend_memory_request` | | `15Mi` |
| `cilium_hubble_ui_frontend_cpu_request` | | `null` |
| `cilium_hubble_ui_frontend_memory_limit` | | `1Gi` |
| `cilium_hubble_ui_frontend_cpu_limit` | | `null` |
| `cilium_operator_memory_request` | | `50Mi` |
| `cilium_operator_cpu_request` | | `2m` |
| `cilium_operator_memory_limit` | | `1Gi` |
| `cilium_operator_cpu_limit` | | `null` |
| `cilium_chart_version` | | :page_with_curl: |
| `cilium_cli_version` | | :page_with_curl: |
| `cilium_hubble_version` | | :page_with_curl: |
| `cilium_image` | | :page_with_curl: |
| `cilium_certgen_image` | | :page_with_curl: |
| `cilium_hubble_relay_image` | | :page_with_curl: |
| `cilium_envoy_image` | | :page_with_curl: |
| `cilium_hubble_ui_backend_image` | | :page_with_curl: |
| `cilium_hubble_ui_image` | | :page_with_curl: |
| `cilium_hubble_proxy_image` | | :page_with_curl: |
| `cilium_operator_image` | | :page_with_curl: |
| `cilium_curl_image` | | :page_with_curl: |
| `cilium_dns_test_server_image` | | :page_with_curl: |
| `cilium_json_mock_image` | | :page_with_curl: |
| `cilium_performance_image` | | :page_with_curl: |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `cilium_projects` |  | :page_with_curl: |
| `cilium_downloads` |  | :page_with_curl: |
