# GitLabracadabra

## Scope

The `gitlabracadabra` role installs GitLabracadabra out of the cluster, used
to synchronize artifacts from internet to [GitLab](gitlab.md) for offline mode.

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/gitlabracadabra.yml).

Public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `gitlabracadabra_external_enabled` | Enable GitLabracadabra | When the `bind` group is not empty: `"{{ groups['gitlabracadabra'] \| default([]) \| length > 0 }}"` |
| `gitlabracadabra_gitlab_username` | GitLab username | `kubitus-gitlabracadabra` |
| `gitlabracadabra_gitlab_name` | GitLab name | `Kubitus GitLabracadabra` |
| `gitlabracadabra_gitlab_email` | GitLab email | `"{{ gitlabracadabra_gitlab_username }}@{{ top_level_domain }}"` |
| `gitlabracadabra_gitlab_password` | GitLab password | `"{{ lookup('password', kubitus_credentials_dir + '/gitlabracadabra_gitlab_password.creds length=32') }}"` |
| `gitlabracadabra_sync_kubitus_installer` | Sync the [Kubitus Installer repository](https://gitlab.com/kubitus-project/kubitus-installer) | `true` |
| `gitlabracadabra_image` | GitLabracadabra image | :page_with_curl: |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `gitlabracadabra_projects` |  | :page_with_curl: |
