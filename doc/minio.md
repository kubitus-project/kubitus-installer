# MinIO

## Scope

The `minio` role installs [MinIO](min.io/), as object storage.

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/minio.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `minio_enabled` | Enable MinIO | `false` |
| `minio_description` | Description | `MinIO` |
| `minio_helm_dependencies` | Helm dependencies | :page_with_curl: |
| `minio_keep_file_paths` | Keep file paths | :page_with_curl: |
| `minio_extra_namespace_resource_whitelist` | Extra namespace resource whitelist | :page_with_curl: |
| `minio_sealed_secrets` | Sealed secrets | `[minio-gitops, minio-secretkeys-gitops, minio-extra-gitops]` |
| `minio_sealed_secret_minio_gitops_items` | `minio-gitops` secret items | `[rootUser, rootPassword]` |
| `minio_instance_default_sealed_secret_minio_gitops_data_rootuser_value` | Root user | `"{{ minio_root_user }}"` |
| `minio_instance_default_sealed_secret_minio_gitops_data_rootpassword_value` | Root password | `"{{ minio_root_password }}"` |
| `minio_sealed_secret_minio_gitops_data` | Access key to secret key mapping | :page_with_curl: |
| `minio_sealed_secret_minio_extra_gitops_items` | `minio-extra-gitops` secret items | `[config.env]` |
| `minio_sealed_secret_minio_extra_gitops_data_config_env_value` | | :page_with_curl: |

Other public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `minio_fqdn` | MinIO DNS name | `"minio.{{ top_level_domain }}"` |
| `minio_ingress_certificate_issuer` | MinIO ingress certificate issuer. See [`default_ingress_certificate_issuer`](kubitus_defaults.md#variables) for possible values | `"{{ default_ingress_certificate_issuer }}"` |
| `minio_console_fqdn` | MinIO console DNS name | `"minio-console.{{ top_level_domain }}"` |
| `minio_console_ingress_certificate_issuer` | MinIO console ingress certificate issuer. See [`default_ingress_certificate_issuer`](kubitus_defaults.md#variables) for possible values | `"{{ default_ingress_certificate_issuer }}"` |
| `minio_root_user` | MinIO root user | :page_with_curl: |
| `minio_root_password` | MinIO root password | :page_with_curl: |
| `minio_drives_per_node` | Number of drives attached to a node | `1` |
| `minio_replicas` | Number of MinIO containers running | `16` |
| `minio_pools` | Number of expanded MinIO clusters | `1` |
| `minio_persistence_storageclass` | Storage class of PV to bind. By default it looks for standard storage class | `""` |
| `minio_persistence_size` | | `500Gi` |
| `minio_memory_request` | [Memory request](https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/) | `16Gi` |
| `minio_memory_limit` | [Memory limit](https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/) | `"{{ minio_memory_request }}"` |
| `minio_policies` | List of MinIO policies. Format is described [here](https://github.com/minio/minio/blob/3a398775fb40525e1b86665a72403a2b02423f92/helm/minio/values.yaml#L252-L291) | :page_with_curl: |
| `minio_users` | List of MinIO users. Format is described [here](https://github.com/minio/minio/blob/3a398775fb40525e1b86665a72403a2b02423f92/helm/minio/values.yaml#L308-L323) | `[{accessKey: "{{ velero_aws_access_key_id }}", secretKey: "{{ velero_aws_secret_access_key }}", policy: readwrite_velero}]` |
| `minio_buckets` | List of MinIO buckets. Format is described [here](https://github.com/minio/minio/blob/3a398775fb40525e1b86665a72403a2b02423f92/helm/minio/values.yaml#L342-L358) | `[{name: velero, policy: none}]` |
| `minio_oidc_enabled` | Enable OIDC | `false` |
| `minio_oidc_config_url` | OIDC config URL | `"https://{{ keycloak_fqdn }}/auth/realms/default/.well-known/openid-configuration"` |
| `minio_oidc_client_id` | OIDC client ID | `minio` |
| `minio_oidc_client_secret` | OIDC client secret | :page_with_curl: |
| `minio_oidc_claim_name` | OIDC claim name | `policy` |
| `minio_oidc_scopes` | OIDC scopes | `openid,profile,email` |
| `minio_oidc_redirect_uri` | OIDC redirect URI | `"https://{{ minio_console_fqdn }}/oauth_callback"` |
| `minio_oidc_claim_prefix` | OIDC claim prefix | `''` |
| `minio_oidc_comment` | OIDC comment | `''` |
| `minio_chart_version` |  | :page_with_curl: |
| `minio_image` |  | :page_with_curl: |
| `minio_client_image` |  | :page_with_curl: |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `minio_projects` |  | :page_with_curl: |
