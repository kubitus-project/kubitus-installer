# Global

## Scope

The `global` role installs various global (cluster-wide) resources:

- `CustomResourceDefinition` for `podmonitors`, `probes`, `prometheusrules`, and `servicemonitors`
- `ClusterRole` and `ClusterRoleBinding`

and resources in default namespaces (`default`, `kube-node-lease`, `kube-public`, and `kube-system`):

- `NetworkPolicy`

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/global_resources.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `global_description` | Description | `Global resources` |
| `global_namespace` | Namespace | `default` |
| `global_extra_namespaces` | Extra namespaces | `"{{ global_netpol_namespaces | union(['kube-system']) | difference(['default']) }}"` |

Other public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `global_netpol_namespaces` | Namespaces where a default `NetworkPolicy` is created (allowing only DNS) | `[default, kube-node-lease, kube-public]` |
| `global_{view,edit,admin,cluster_admin,cluster_viewer}_groups` | Groups with the corresponding `ClusterRole` | `[]` |
| `global_{view,edit,admin,cluster_admin,cluster_viewer}_users` | Users with the corresponding `ClusterRole` | `[]` |
| `global_impersonate_{view,edit,admin,cluster_admin,cluster_viewer}_groups` | Groups with ability to run with corresponding `--as={view,edit,admin,cluster-admin,cluster-viewer}` user | `[]` |
| `global_impersonate_{view,edit,admin,cluster_admin,cluster_viewer}_users` | Users with ability to run with corresponding `--as={view,edit,admin,cluster-admin,cluster-viewer}` user | `[]` |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `global_prometheus_operator_crds` |  | :page_with_curl: |
| `global_projects` |  | :page_with_curl: |
