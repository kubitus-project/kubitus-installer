# Terraform

## Scope

The `terraform` role creates compute instances using
[Terraform](https://www.terraform.io/).

For GKE, node pools are constructed from fake hosts in the `gke_node_pools` group of the inventory. Example:

```yaml
# inventory.yml
gke_node_pools:
  hosts:
    pool1:
      terraform_gke_machine_type: n1-standard-4
    pool2:
      terraform_gke_preemptible: true
```

For offline mode, files should be copied manually. For example with `vsphere`:

- download and install terraform manually on the node (and set `terraform_setup_repo: false`)
- download needed modules respecting convention. For example:

  ```
  /path/to/modules/registry.terraform.io/hashicorp/local/terraform-provider-local_2.2.0_linux_amd64.zip
  /path/to/modules/registry.terraform.io/hashicorp/vsphere/terraform-provider-vsphere_2.2.0_linux_amd64.zip
  ```

- Create a config file :

  ```hcl
  # ../inventories/$inventory/terraform-vsphere/terraform.tfrc
  provider_installation {
    filesystem_mirror {
      path = "/path/to/modules"
      include = ["*/*/*"]
    }
  }
  ```


## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/terraform.yml).

Public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `terraform_external_enabled` | Enable Terraform | `true` |
| `terraform_version` | Terraform version | :page_with_curl: |
| `terraform_setup_repo` | Setup APT repository  | `true` |
| `terraform_apply_mode` | `apply` or `destroy` | `apply` |
| `terraform_ssh_pub_key` | Path to SSH public key | `"~/.ssh/id_rsa.pub"` |
| `terraform_ssh_whitelist` | List of IPs allowed to SSH. Example: `[10.20.30.40]` | `[]` |
| `terraform_api_server_whitelist` | List of IPs with access to API | `[]` |
| `terraform_nodeport_whitelist` | List of IPs with access to nodeports | `[]` |
| `terraform_ingress_whitelist` | List of IPs with access to ingress | `[0.0.0.0/0]` |
| `terraform_ssh_user` | User to SSH. Cannot be changed for `gcp` | `ubuntu` |
| `terraform_boot_disk_size` | Boot disk size in GB | `50` |
| `terraform_additional_disks` | Extra disks, with size in GB. Example: `{extra-disk-1: {size: 100}}` | `{}` |
| `terraform_all_nodes` | List of nodes to create | `"{{ groups['all'] \| difference(['localhost']) \| difference(groups['gke_node_pools'] \| default([])) \| difference(groups['clusters'] \| default([])) }}"` |

Public variables for `GCP`:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `terraform_gcp_enabled` | Enable GCP provider | `false` |
| `terraform_gcp_nodes` | List of nodes to create on GCP | `"{{ terraform_all_nodes }}"` |
| `terraform_gcp_directory` | Where GCP data is stored. Contains `tfvars.json` and `terraform.tfstate` | `"{{ inventory_dir }}/terraform-gcp"` |
| `terraform_gcp_project_id` | GCP project id | `kubitus-installer` |
| `terraform_gcp_region` | GCP region | `europe-west1` |
| `terraform_gcp_zone` | GCP zone | `europe-west1-b` |
| `terraform_gcp_prefix` | Prefix applied to all resource names | `kubitus` |
| `terraform_gcp_keyfile_location` | Path to GCP keyfile | `"project.json"` |
| `terraform_gcp_controlplane_serviceaccount_email` | | `null` |
| `terraform_gcp_controlplane_serviceaccount_scopes` | | `[]` |
| `terraform_gcp_controlplane_preemptible` | | `false` |
| `terraform_gcp_controlplane_additional_disk_type` | | `pd-ssd` |
| `terraform_gcp_worker_serviceaccount_email` | | `null` |
| `terraform_gcp_worker_serviceaccount_scopes` | | `[]` |
| `terraform_gcp_worker_preemptible` | | `false` |
| `terraform_gcp_worker_additional_disk_type` | | `pd-ssd` |
| `terraform_gcp_boot_disk_image_name` | | `projects/debian-cloud/global/images/debian-12-bookworm-v20240213` |
| `terraform_gcp_node_size` | | `n1-standard-2` |
| `terraform_gcp_extra_ingress_firewalls` | Format is described [here](../roles/terraform/files/from_kubespray/gcp/README.md) | `{}` |

Public variables for `GKE`:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `terraform_gke_enabled` | Enable GKE provider | `false` |
| `terraform_gke_directory` | Where GKE data is stored. Contains `tfvars.json`, `terraform.tfstate` and `kubeconfig` | `"{{ inventory_dir }}/terraform-gke"` |
| `terraform_gke_project_id` | GCP project id used for GKE | `kubitus-installer` |
| `terraform_gke_location` | Zone or Region | `europe-west1-b` |
| `terraform_gke_deletion_protection` | Prevent Terraform from destroying the cluster | `true` |
| `terraform_gke_prefix` | Prefix applied to all resource names | `kubitus` |
| `terraform_gke_release_channel` | Selected [release channel](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/container_cluster#channel) | `REGULAR` |
| `terraform_gke_keyfile_location` | Path to GCP keyfile used for GKE | `"project.json"` |
| `terraform_gke_serviceaccount_email` | | `null` |
| `terraform_gke_network` | Network name (should exists) | `default` |
| `terraform_gke_subnetwork` | Subnet name (should exists) | `default` |
| `terraform_gke_initial_node_count` | | `1` |
| `terraform_gke_min_node_count` | | `1` |
| `terraform_gke_max_node_count` | | `10` |
| `terraform_gke_machine_type` | | `n1-standard-2` |
| `terraform_gke_preemptible` | | `false` |
| `terraform_gke_disk_size_gb` | | `50` |
| `terraform_gke_kubeconfig` | Location of created kubeconfig | `"{{ terraform_gke_directory }}/kubeconfig"` |

Public variables for `vSphere`:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `terraform_vsphere_enabled` | Enable vSphere provider | `false` |
| `terraform_vsphere_nodes` | List of nodes to create on vSphere | `"{{ terraform_all_nodes }}"` |
| `terraform_vsphere_directory` | Where vSphere data is stored. Contains `tfvars.json` and `terraform.tfstate` | `"{{ inventory_dir }}/terraform-vsphere"` |
| `terraform_vsphere_prefix` | Prefix applied to all resource names | `kubitus` |
| `terraform_vsphere_network` | VM network. Example `/datacenter1/network/dvswitch1/portgroup1` | `VM Network` |
| `terraform_vsphere_ip` | VM IP. Example : `10.1.2.3` | `"{{ ip }}"` |
| `terraform_vsphere_netmask` | VM netmask. Example: `24` | `"{{ netmask }}"` |
| `terraform_vsphere_gateway` | VM gateway. Example : `10.1.2.1` | `"{{ gateway }}"` |
| `terraform_vsphere_ssh_public_keys` | List of SSH `authorized_keys` lines | `[]` |
| `terraform_vsphere_datacenter` | vSphere datacenter | `null` |
| `terraform_vsphere_compute_cluster` | vSphere cluster | `null` |
| `terraform_vsphere_datastore` | vSphere datastore | `null` |
| `terraform_vsphere_user` | vSphere API login | `null` |
| `terraform_vsphere_password` | vSphere API password | `null` |
| `terraform_vsphere_server` | vSphere hostname | `null` |
| `terraform_vsphere_template_name` | VM template name | `null` |
| `terraform_vsphere_template_folder` | VM destination folder | `""` |
| `terraform_vsphere_hardware_version` | VM hardware version | `15` |
