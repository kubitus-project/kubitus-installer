# NGINX ingress controller

## Scope

The `ingress_nginx` role installs the [NGINX ingress controller](https://kubernetes.github.io/ingress-nginx/),
managing Kubernetes Ingress resources.

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/ingress_nginx.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `ingress_nginx_enabled` | Enable NGINX ingress controller | `true` |
| `ingress_nginx_description` | Description | `NGINX ingress controller` |
| `ingress_nginx_helm_dependencies` | Helm dependencies | :page_with_curl: |
| `ingress_nginx_keep_file_paths` | Keep file paths | :page_with_curl: |
| `ingress_nginx_extra_cluster_resource_whitelist` | Extra cluster resource whitelist | :page_with_curl: |
| `ingress_nginx_extra_namespace_resource_whitelist` | Extra namespace resource whitelist | :page_with_curl: |
| `ingress_nginx_sealed_secrets` | Sealed secrets | `[default-ssl-certificate]` |
| `ingress_nginx_sealed_secret_default_ssl_certificate_type` |  `default-ssl-certificate` secret type | `kubernetes.io/tls` |
| `ingress_nginx_sealed_secret_default_ssl_certificate_items` | `default-ssl-certificate` secret items | `[ca.crt, tls.crt, tls.key]` |
| `ingress_nginx_sealed_secret_default_ssl_certificate_data_ca_crt_value` | `ca.crt` content | :page_with_curl: |
| `ingress_nginx_sealed_secret_default_ssl_certificate_data_tls_key_value` | `tls.key` content | :page_with_curl: |
| `ingress_nginx_sealed_secret_default_ssl_certificate_data_tls_crt_value` | `tls.crt` content | :page_with_curl: |

Other public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `ingress_nginx_controller_service_type` | NGINX ingress controller service type | `LoadBalancer` |
| `ingress_nginx_controller_host_network` | NGINX ingress controller host network | `"{{ ingress_nginx_controller_service_type == 'ClusterIP' }}"` |
| `ingress_nginx_load_balancer_ip` | NGINX ingress controller LoadBalancer IP | `null` |
| `ingress_nginx_default_certificate_fqdn` | FQDN of the default TLS certificate | `"*.{{ top_level_domain }}"` |
| `ingress_nginx_allow_snippet_annotations` | Allow snippet annotations | `true` |
| `ingress_nginx_annotation_value_word_blocklist` | See [annotation-value-word-blocklist](https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/configmap/#annotation-value-word-blocklist) | `"load_module,lua_package,_by_lua,location,root,proxy_pass,serviceaccount,{,},',\""` |
| `ingress_nginx_annotations_risk_level` | See [annotations-risk-level](https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/configmap/#annotations-risk-level) | `Critical` |
| `ingress_nginx_client_max_body_size` | Custom max body size (proxy-body-size) | `0` |
| `ingress_nginx_keepalive_timeout` | Set the timeout during which a keep-alive client connection will stay open ont the server side (upstream-keepalive-timeout) | `10` |
| `ingress_nginx_ssl_protocols` | SSL enabled protocols to use (ssl-protocols) | `TLSv1.3` |
| `ingress_nginx_ssl_ciphers` | Specifies the enabled ciphers (ssl-ciphers) | `EECDH+AESGCM:EDH+AESGCM` |
| `ingress_nginx_hsts_preload` | Enabled the preload attribute in the HSTS feature (hsts-preload) | `true` |
| `ingress_nginx_enabled_ocsp` | Enabled Online Certificate Status Protocol stapling (enabled-ocsp) | `true` |
| `ingress_nginx_client_header_timeout` | Defines a timeout for reading client request header, in seconds (client-header-timeout) | `60` |
| `ingress_nginx_client_body_buffer_size` | Sets buffer size for reading client request body (client-body-buffer-size) | `1m` |
| `ingress_nginx_large_client_header_buffers` | Sets the maximum number and size of buffers for reading large client request header (large-client-header-buffers) | `4 8k` |
| `ingress_nginx_proxy_hide_header` | Sets additional header thet will not be passed from the upstream server to the cient response (hide-headers) | `[Server, X-Powered-By]` |
| `ingress_nginx_defaultbackend_custom_error_enabled` | Enabled custom error pages | `false` |
| `ingress_nginx_limit_rate` | Limits the rate (bytes per second) of response transmission to a client (limit-rate) (The zero value disables rate limiting) | `0` |
| `ingress_nginx_limit_req_status_code` | Sets the status code to return in response to rejected requests | `429` |
| `ingress_nginx_limit_conn_status_code` | Sets the status code to return in respsonse to rejected connections | `429` |
| `ingress_nginx_log_format_escape_json` | Log format escape json | `true` |
| `ingress_nginx_log_format_upstream` | Log format upstream | `'{"time": "$time_iso8601", "remote_addr": "$remote_addr", "realip_remote_addr": "$realip_remote_addr", "request_id": "$req_id", "remote_user": "$remote_user", "bytes_sent": $bytes_sent, "request_time": $request_time, "status": $status, "vhost": "$host", "request_proto": "$server_protocol", "path": "$uri", "request_query": "$args", "request_length": $request_length, "duration": $request_time, "method": "$request_method", "http_referrer": "$http_referer", "http_user_agent": "$http_user_agent", "proxy_upstream_name": "$proxy_upstream_name", "proxy_alternative_upstream_name": "$proxy_alternative_upstream_name", "upstream_addr": "$upstream_addr", "upstream_response_length": "$upstream_response_length", "upstream_response_time": "$upstream_response_time", "upstream_status": "$upstream_status", "server_port": $server_port, "namespace": "$namespace"}'` |
| `ingress_nginx_from_cidrs` | Allowed IP subnets (when set) | `[]` |
| `ingress_nginx_from_port80_cidrs` | Allowed IP subnets (when set) access to port 80 | `[]` |
| `ingress_nginx_worker_processes` | Number of worker processes | `auto` |
| `ingress_nginx_global_allowed_response_headers` | `global-allowed-response-headers` | `[Cache-Control, Content-Security-Policy, Content-Security-Policy-Report-Only, Reporting-Endpoints, Strict-Transport-Security, X-Content-Type-Options, X-Frame-Options]` |
| `ingress_nginx_extra_config` | Extra config. See [doc](https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/configmap/) | `{}` |

Alerts variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `ingress_nginx_status_408_warning_threshold` | Warning threshold for HTTP status 408 (Request Timeout) errors | `5` |
| `ingress_nginx_status_408_critical_threshold` | Critical threshold for HTTP status 408 (Request Timeout) errors | `10` |
| `ingress_nginx_status_422_warning_threshold` | Warning threshold for HTTP status 422 (Unprocessable Entity) errors | `5` |
| `ingress_nginx_status_422_critical_threshold` | Critical threshold for HTTP status 422 (Unprocessable Entity) errors | `10` |
| `ingress_nginx_status_499_warning_threshold` | Warning threshold for HTTP status 499 (Client Closed Connection) errors | `5` |
| `ingress_nginx_status_499_critical_threshold` | Critical threshold for HTTP status 499 (Client Closed Connection) errors | `10` |
| `ingress_nginx_status_500_warning_threshold` | Warning threshold for HTTP status 500 (Internal Server Error) errors | `5` |
| `ingress_nginx_status_500_critical_threshold` | Critical threshold for HTTP status 500 (Internal Server Error) errors | `10` |
| `ingress_nginx_status_502_warning_threshold` | Warning threshold for HTTP status 502 (Bad Gateway) errors | `5` |
| `ingress_nginx_status_502_critical_threshold` | Critical threshold for HTTP status 502 (Bad Gateway) errors | `10` |
| `ingress_nginx_status_503_warning_threshold` | Warning threshold for HTTP status 503 (Service Unavailable) errors | `5` |
| `ingress_nginx_status_503_critical_threshold` | Critical threshold for HTTP status 503 (Service Unavailable) errors | `10` |
| `ingress_nginx_status_504_warning_threshold` | Warning threshold for HTTP status 504 (Gateway Timeout) errors | `5` |
| `ingress_nginx_status_504_critical_threshold` | Critical threshold for HTTP status 504 (Gateway Timeout) errors | `10` |


Resources variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `ingress_nginx_controller_memory_request` | | `500Mi` |
| `ingress_nginx_controller_cpu_request` | | `40m` |
| `ingress_nginx_controller_memory_limit` | | `1Gi` |
| `ingress_nginx_controller_cpu_limit` | | `null` |
| `ingress_nginx_defaultbackend_memory_request` | | `12Mi` |
| `ingress_nginx_defaultbackend_cpu_request` | | `5m` |
| `ingress_nginx_defaultbackend_memory_limit` | | `16Mi` |
| `ingress_nginx_defaultbackend_cpu_limit` | | `null` |
| `ingress_nginx_createsecretjob_memory_request` | | `20Mi` |
| `ingress_nginx_createsecretjob_cpu_request` | | `10m` |
| `ingress_nginx_createsecretjob_memory_limit` | | `20Mi` |
| `ingress_nginx_createsecretjob_cpu_limit` | | `10m` |
| `ingress_nginx_patchwebhookjob_memory_request` | | `20Mi` |
| `ingress_nginx_patchwebhookjob_cpu_request` | | `10m` |
| `ingress_nginx_patchwebhookjob_memory_limit` | | `20Mi` |
| `ingress_nginx_patchwebhookjob_cpu_limit` | | `10m` |

Artifact variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `ingress_nginx_chart_version` |  | :page_with_curl: |
| `ingress_nginx_controller_image` |  | :page_with_curl: |
| `ingress_nginx_kube_webhook_certgen_image` |  | :page_with_curl: |
| `ingress_nginx_defaultbackend_image` |  | :page_with_curl: |
| `ingress_nginx_errors_image` |  | :page_with_curl: |
| `ingress_nginx_http_error_threshold` | Threshold for Alertmanager rules | `5` |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `ingress_nginx_projects` |  | :page_with_curl: |
