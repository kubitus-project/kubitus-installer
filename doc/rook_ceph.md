# Rook-Ceph

## Scope

The `rook_ceph` role installs [Rook](https://rook.github.io/), providing `PersistentVolume`s.

## Accessing Rook-Ceph

The Rook-Ceph dashboard `"admin"` password is available from a secret:

```console
$ ROOK_CEPH_DASHBOARD_PASSWORD="$(kubectl get secrets -n rook-ceph rook-ceph-dashboard-password -ojson | jq .data.password -r | base64 -d)"
$ echo $ROOK_CEPH_DASHBOARD_PASSWORD
AbCdEf
```

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/rook_ceph.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `rook_ceph_enabled` | Enable Rook-Ceph | `true` |
| `rook_ceph_description` | Description | `true` |
| `rook_ceph_namespace_pod_security` | Pod Security of the instance namespace | `privileged` |
| `rook_ceph_helm_dependencies` | Helm dependencies | :page_with_curl: |
| `rook_ceph_server_side_apply` | Server-side apply | `true` |
| `rook_ceph_extra_cluster_resource_whitelist` | Extra cluster resource whitelist | :page_with_curl: |
| `rook_ceph_extra_namespace_resource_whitelist` | Extra namespace resource whitelist | :page_with_curl: |

Other public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `rook_ceph_enabled` | Enable Rook-Ceph | `true` |
| `rook_ceph_cephfs_enabled` | Create a CephFS StorageClass | `false` |
| `rook_ceph_objectstore_enabled` | Enable object store | `false` |
| `rook_ceph_storage_use_all_nodes` | `true` or `false`, indicating if all nodes in the cluster should be used for storage according to the cluster level storage selection and configuration values. See [cluster settings](https://rook.io/docs/rook/v1.9/ceph-cluster-crd.html#cluster-settings) | `true` |
| `rook_ceph_storage_use_all_devices` | `true` or `false`, indicating whether all devices found on nodes in the cluster should be automatically consumed by OSDs. **Not recommended** unless you have a very controlled environment where you will not risk formatting of devices with existing data. When `true`, all devices/partitions will be used. Is overridden by `deviceFilter` if specified. See [storage selection settings](https://rook.io/docs/rook/v1.9/ceph-cluster-crd.html#storage-selection-settings) | `false` |
| `rook_ceph_storage_device_filter` | A regular expression for short kernel names of devices (e.g. `sda`) that allows selection of devices to be consumed by OSDs. See [storage selection settings](https://rook.io/docs/rook/v1.9/ceph-cluster-crd.html#storage-selection-settings) | `'^sd[^a-e]'` |
| `rook_ceph_remove_osds_if_out_and_safe_to_remove` | `removeOSDsIfOutAndSafeToRemove`: If `true` the operator will remove the OSDs that are down and whose data has been restored to other OSDs. In Ceph terms, the OSDs are `out` and `safe-to-destroy` when they are removed. See [cluster settings](https://rook.io/docs/rook/v1.9/ceph-cluster-crd.html#cluster-settings) | `false` |
| `rook_ceph_mgr_dashboard_fqdn` | Rook-Ceph mgr dashboard DNS name | `"rook-ceph.{{ top_level_domain }}"` |
| `rook_ceph_mgr_dashboard_ingress_certificate_issuer` | Rook-Ceph mgr dashboard ingress certificate issuer. See [`default_ingress_certificate_issuer`](kubitus_defaults.md#variables) for possible values | `"{{ default_ingress_certificate_issuer }}"` |

Resources requests and limits:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `rook_ceph_operator_memory_request` | | `null` |
| `rook_ceph_operator_cpu_request` | | `null` |
| `rook_ceph_operator_memory_limit` | | `null` |
| `rook_ceph_operator_cpu_limit` | | `null` |
| `rook_ceph_toolbox_memory_request` | | `null` |
| `rook_ceph_toolbox_cpu_request` | | `null` |
| `rook_ceph_toolbox_memory_limit` | | `null` |
| `rook_ceph_toolbox_cpu_limit` | | `null` |
| `rook_ceph_cluster_spec_resource_overrides` | See [here](https://github.com/rook/rook/blob/e72616e/deploy/charts/rook-ceph-cluster/values.yaml#L272-L333) | `{}` |

Versions:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `rook_ceph_chart_version` | | :page_with_curl: |
| `rook_ceph_cluster_chart_version` | | :page_with_curl: |
| `rook_ceph_operator_image` | | :page_with_curl: |
| `rook_ceph_ceph_image` | | :page_with_curl: |
| `rook_ceph_cephcsi_image` | | :page_with_curl: |
| `rook_ceph_csi_registrar_image` | | :page_with_curl: |
| `rook_ceph_csi_resizer_image` | | :page_with_curl: |
| `rook_ceph_csi_provisioner_image` | | :page_with_curl: |
| `rook_ceph_csi_snapshotter_image` | | :page_with_curl: |
| `rook_ceph_csi_attacher_image` | | :page_with_curl: |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `rook_ceph_projects` |  | :page_with_curl: |
