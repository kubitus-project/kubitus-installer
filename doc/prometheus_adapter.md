# Prometheus Adapter

## Scope

The `prometheus_adapter` role installs [Prometheus Adapter](https://github.com/kubernetes-sigs/prometheus-adapter),
an implementation of the custom.metrics.k8s.io API using Prometheus.

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/prometheus_adapter.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `prometheus_adapter_enabled` | Enable Prometheus Adapter | `false` |
| `prometheus_adapter_description` | Description | `Prometheus Adapter` |
| `prometheus_adapter_extra_namespaces` | Extra namespaces | `[kube-system]` |
| `prometheus_adapter_helm_dependencies` | Helm dependencies | :page_page_with_curl: |
| `prometheus_adapter_extra_cluster_resource_whitelist` | Extra cluster resource whitelist | :page_page_with_curl: |

Other public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `prometheus_adapter_memory_request` | Memory request | `1024Mi` |
| `prometheus_adapter_memory_limit` | Memory limit | `3Gi` |
| `prometheus_adapter_cpu_request` | CPU request | `100m` |
| `prometheus_adapter_cpu_limit` | CPU limit | `2000m` |
| `prometheus_adapter_default_rules_enabled` | Enable default rules | `true` |
| `prometheus_adapter_custom_rules` | Custom rules | `[]` |
| `prometheus_adapter_external_rules` | External rules | `[]` |
| `prometheus_adapter_default_resource_rules_enabled` | Enable default resource rules | `true` |
| `prometheus_adapter_resource_rules` | Resource rules | `[]` |
| `prometheus_adapter_chart_version` | | :page_with_curl: |
| `prometheus_adapter_image` | | :page_with_curl: |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `prometheus_adapter_default_resource_rules` | | :page_with_curl: |
| `prometheus_adapter_projects` | | :page_with_curl: |
