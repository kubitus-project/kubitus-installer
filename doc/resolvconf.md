# Resolvconf

## Scope

The `resolvconf` role manages `/etc/resolv.conf`.

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/resolvconf.yml).

Public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `resolvconf_external_enabled` | Enable resolvconf | `"{{ bind_external_enabled }}"` |
| `resolvconf_dnssec` | When not `null`, sets [DNSSEC option](https://www.freedesktop.org/software/systemd/man/resolved.conf.html#DNSSEC=) in `/etc/systemd/resolved.conf` | `null`|
