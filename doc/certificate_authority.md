# PKI

## Scope

The `certificate_authority` role creates several Certificate Authorities.

## CAs

Example:

```yaml
certificate_authority_extra_authorities:
- name: apiserver-cluster-foo-authenticating-proxy
  certificates:
  - name: apiserver-cluster-foo-authenticating-proxy-client-certificate
```

CA variables :

| Name | Description | Default |
| ---- | ----------- | ------- |
| `name` | CA name | :warning: Mandatory |
| `ca_common_name` | CA common name | `"{{ authority.name }}"` |
| `ca_passphrase` | CA passphrase | `"{{ lookup('password', kubitus_credentials_dir + '/ca_passphrase_' + authority.name  + '.creds length=32 chars=ascii_letters,digits') }}"` |
| `ca_not_after` | The point in time at which the CA stops being valid | `'+3650d'` |
| `certificates` | Certificates, see below | `[]` |

Certificate variables :

| Name | Description | Default |
| ---- | ----------- | ------- |
| `name` | Certificate name | :warning: Mandatory |
| `common_name` | Certificate Common Name (CN) | `"{{ certificate.name }}"` |
| `subject_alt_name` | Certificate Subject alternative name | `[]` |
| `not_after` | The point in time at which the certificate stops being valid | `'+365d'` |
| `not_before` | The point in time the certificate is valid from | `'-1d'` |

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/pki.yml).

Public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `certificate_authority_enabled` | Enable Certificate Authorities | `true` |
| `certificate_authority_extra_authorities` | Extra CAs. See [above](#cas) | `[]` |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `certificate_authority_authorities` |  | :page_with_curl: |
