# Opencost

## Scope

The `opencost` role installs [Opencost](https://opencost.io/docs/), as cost monitoring tool.

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/opencost.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `opencost_enabled` | Enable Opencost | `false` |
| `opencost_description` | Description | `OpenCost` |
| `opencost_helm_dependencies` | Helm dependencies | :page_with_curl: |

Resources variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `opencost_exporter_cpu_request` | | `10m` |
| `opencost_exporter_memory_request` | | `55Mi` |
| `opencost_exporter_cpu_limit` | | `999m` |
| `opencost_exporter_memory_limit` | | `1Gi` |
| `opencost_ui_cpu_request` | | `10m` |
| `opencost_ui_memory_request` | | `55Mi` |
| `opencost_ui_cpu_limit` | | `999m` |
| `opencost_ui_memory_limit` | | `1Gi` |

Other public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `opencost_ingress_enabled` | Enable Ingress Opencost UI | `false` |
| `opencost_fqdn` | Opencost DNS name | `"opencost.{{ top_level_domain }}"` |
| `opencost_ingress_certificate_issuer` | Opencost ingress certificate issuer. See [`default_ingress_certificate_issuer`](kubitus_defaults.md#variables) for possible values | `"{{ default_ingress_certificate_issuer }}"` |
| `opencost_chart_version` |  | :page_with_curl: |
| `opencost_exporter_image` | | :page_with_curl: |
| `opencost_ui_image` | | :page_with_curl: |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `opencost_projects` |  | :page_with_curl: |
