# Govc

## Scope

The `govc` role installs `govc`.

This role is applied on the `govc` group.

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/govc.yml).

Public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `govc_enabled` | Enable Govc installation | `true` |
| `govc_version` |  | :page_with_curl: |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `govc_projects` |  | :page_with_curl: |
| `govc_downloads` |  | :page_with_curl: |
