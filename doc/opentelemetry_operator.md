# OpenTelemetry Operator

## Scope

The `opentelemetry_operator` role installs [OpenTelemetry Operator](https://github.com/open-telemetry/opentelemetry-operator),
and create OpenTelemetry collectors.

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/opentelemetry_operator.yml).

GitOps variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `opentelemetry_operator_enabled` | Enable OpenTelemetry Operator | `false` |
| `opentelemetry_operator_description` | Description | `OpenTelemetry Operator` |
| `opentelemetry_operator_helm_dependencies` | Helm dependencies | :page_with_curl: |

Versions variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `opentelemetry_operator_chart_version` |  | :page_with_curl: |
| `opentelemetry_operator_image` |  | :page_with_curl: |
| `opentelemetry_operator_collector_image` |  | :page_with_curl: |
| `opentelemetry_operator_target_allocator_image` |  | :page_with_curl: |
| `opentelemetry_operator_opamp_bridge_image` |  | :page_with_curl: |
| `opentelemetry_operator_java_instrumentation_image` |  | :page_with_curl: |
| `opentelemetry_operator_nodejs_instrumentation_image` |  | :page_with_curl: |
| `opentelemetry_operator_python_instrumentation_image` |  | :page_with_curl: |
| `opentelemetry_operator_dotnet_instrumentation_image` |  | :page_with_curl: |
| `opentelemetry_operator_go_instrumentation_image` |  | :page_with_curl: |
| `opentelemetry_operator_kube_rbac_proxy_image` |  | :page_with_curl: |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `opentelemetry_operator_projects` |  | :page_with_curl: |
