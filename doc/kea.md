# Kea

## Scope

The `kea` role installs an Kea-DHCP server.

:warning: The default range is from `.10` to `.40` in the subnet of the default interface. You can tune this with either:

- `kea_default_range_start` and `kea_default_range_size`, or
- `kea_default_range`, or
- `kea_subnets`

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/dhcp.yml).

Public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `kea_external_enabled` | Enable Kea-DCHP (only on group `kea`) | `true` |
| `kea_interfaces_v4` | List of interfaces to listen to | `["{{ ansible_facts.default_ipv4.interface }}"]` |
| `kea_default_range_start` | Range start for the default pool | `10` |
| `kea_default_range_size` | Range size for the default pool | `30` |
| `kea_default_range` | Range for the default pool | :page_with_curl: |
| `kea_default_ntp_servers` | NTP servers for the default pool | `["{{ ansible_facts.default_ipv4.gateway }}"]` |
| `kea_subnet4` | `subnet4` configuration | :page_with_curl: |
