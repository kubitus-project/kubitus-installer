# kind

## Scope

The `kind` role installs [`kind`](https://kind.sigs.k8s.io/),
a tool for running local Kubernetes clusters using Docker container “nodes”.

## Variables

For variables where the default is not here (:page_with_curl:), see [all defaults](../roles/kubitus_defaults/defaults/main/kind.yml).

Public variables:

| Name | Description | Default |
| ---- | ----------- | ------- |
| `kind_external_enabled` | Enable `kind` (only on group `kind`) | `true` |
| `kind_version` |  | :page_with_curl: |
| `kind_node_image` |  | :page_with_curl: |

Private variables (do not override):

| Name | Description | Default |
| ---- | ----------- | ------- |
| `kind_projects` |  | :page_with_curl: |
| `kind_downloads` |  | :page_with_curl: |
