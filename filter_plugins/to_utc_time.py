__metaclass__ = type

DOCUMENTATION = r'''
  name: to_utc_time
  short_description: convert to utc time
  description:
    - Convert given time to UTC time
  positional: _input, time
  options:
    time:
      description: Hours and minutes. Example '8:00'.
      type: str
      required: true
'''

EXAMPLES = r'''
    utc_time: "{{ '8:00' | to_utc_time }}"
    #  6:00
'''

RETURN = r'''
  _value:
    description:
      - A string of the UTC time.
    type: any
'''

from datetime import datetime

class FilterModule(object):
    def filters(self):
        return {
            'to_utc_time': self.to_utc_time,
        }

    def to_utc_time(self, time):
        now = datetime.now()
        target = '{0}-{1}-{2} {3}'.format(now.year, now.month, now.day, time)
        t_local = datetime.strptime(target, '%Y-%m-%d %H:%M').astimezone()
        return (t_local - t_local.utcoffset()).strftime('%H:%M')
