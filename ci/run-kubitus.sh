#!/bin/bash

set -e
set -o pipefail

# =================================================================
# Functions
# =================================================================
. "$(dirname "$0")/functions.sh"

# =================================================================
# README.md#installation
# =================================================================
section_start install_req \
  'Install required packages'
# =================================================================
(
  set -x
  apt-get install -y ansible git golang-cfssl python3-passlib
)

section_end install_req

# =================================================================
section_start clone_repo \
  'Clone this repository'
# =================================================================
echo Already cloned

section_end clone_repo

# README.md#usage
# =================================================================
section_start create_an_inventory \
  'Create an inventory'
# =================================================================
inventory=foobar
cp -a inventories/sample "inventories/$inventory"

cp -a ci/inventories/$INVENTORY/* "inventories/$inventory"

cat <<EOF > ansible.cfg
[ssh_connection]
pipelining=True
ssh_args = -o ControlMaster=auto -o ControlPersist=30m -o ConnectionAttempts=100 -o UserKnownHostsFile=/dev/null
#control_path = ~/.ssh/ansible-%%r@%%h:%%p

[defaults]
inventory = inventories/$inventory/inventory.yml
host_key_checking=False
inject_facts_as_vars = False
interpreter_python = auto
stdout_callback = yaml
callbacks_enabled = profile_tasks
EOF

section_end create_an_inventory

# =================================================================
section_start external_components \
  'External components'
# =================================================================
(
  set -x
  ansible-playbook external_components.yml
)
section_end external_components

if [ "$INVENTORY" != 'gke' ]; then
  # =================================================================
  section_start kubespray \
    "Kubespray"
  # =================================================================
  (
    set -x
    kubespraynode_ip="$(grep ^ansible_host: inventories/$inventory/host_vars/kubespraynode/from_terraform.yml | cut -d'"' -f2)"
    scp -o StrictHostKeyChecking=no -rp ~/.ssh "ubuntu@$kubespraynode_ip:ssh-config"
    ssh "ubuntu@$kubespraynode_ip" sudo mv /home/ubuntu/ssh-config /home/kubitus/.ssh
    ssh "ubuntu@$kubespraynode_ip" sudo chown -R kubitus /home/kubitus/.ssh

    scp -p ci/run-kubespray.sh "ubuntu@$kubespraynode_ip:/tmp"

    ssh "ubuntu@$kubespraynode_ip" sudo -u kubitus /tmp/run-kubespray.sh
  )

  section_end kubespray
fi

# =================================================================
section_start apps \
  'Apps'
# =================================================================
(
  set -x
  ansible-playbook apps.yml
)
section_end apps

# =================================================================
section_start selfcheck \
  'Selfcheck'
# =================================================================
(
  set -x
  ansible-playbook selfcheck.yml
)
section_end selfcheck
