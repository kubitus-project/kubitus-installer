# =================================================================
# Functions
# =================================================================
section_start() {
  local section_name="$1"
  shift
  echo -e "section_start:`date +%s`:$section_name[collapsed=true]\r\e[0K" "$@"
}

section_end() {
  local section_name="$1"
  shift
  echo -e "section_end:`date +%s`:$section_name\r\e[0K"
}
