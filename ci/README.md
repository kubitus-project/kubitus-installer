# CI setup

## Google Cloud setup

Create :

- A project:
  - Name: `kubitus-installer-e2e`

## GCP setup

Create :

- A [role](https://console.cloud.google.com/iam-admin/roles):
  - Title: `Kubitus Installer e2e GCP`
  - ID: `kubitus_installer_e2e_gcp`
  - Description: `https://gitlab.com/kubitus-project/kubitus-installer/-/blob/main/ci/README.md#gcp-setup`
  - Role launch stage: `Beta`
  - <summary>
    Permissions:
    <details>

    - `compute.addresses.create`
    - `compute.addresses.delete`
    - `compute.addresses.get`
    - `compute.addresses.setLabels`
    - `compute.addresses.use`
    - `compute.disks.create`
    - `compute.disks.delete`
    - `compute.disks.get`
    - `compute.disks.setLabels`
    - `compute.disks.use`
    - `compute.firewalls.create`
    - `compute.firewalls.delete`
    - `compute.firewalls.get`
    - `compute.forwardingRules.create`
    - `compute.forwardingRules.delete`
    - `compute.forwardingRules.get`
    - `compute.forwardingRules.setLabels`
    - `compute.instances.attachDisk`
    - `compute.instances.create`
    - `compute.instances.delete`
    - `compute.instances.detachDisk`
    - `compute.instances.get`
    - `compute.instances.setMetadata`
    - `compute.instances.setServiceAccount`
    - `compute.instances.setTags`
    - `compute.instances.use`
    - `compute.networks.create`
    - `compute.networks.delete`
    - `compute.networks.get`
    - `compute.networks.updatePolicy`
    - `compute.regionOperations.get`
    - `compute.subnetworks.create`
    - `compute.subnetworks.delete`
    - `compute.subnetworks.get`
    - `compute.subnetworks.use`
    - `compute.subnetworks.useExternalIp`
    - `compute.targetPools.create`
    - `compute.targetPools.delete`
    - `compute.targetPools.get`
    - `compute.targetPools.use`
    - `compute.zoneOperations.get`
    - `compute.zones.get`
    </details>
    </summary>
- A [service account](https://console.cloud.google.com/iam-admin/serviceaccounts):
  - Name: `Kubitus Installer e2e GCP`
  - Description: `https://gitlab.com/kubitus-project/kubitus-installer/-/blob/main/ci/README.md#gcp-setup`
  - with a service account key
  - with permissions:
    - Role: `Kubitus Installer e2e GCP`

      Condition `Name starts with kubitus-`:

      ```cel
      resource.name.startsWith("projects/kubitus-installer-e2e/global/networks/kubitus-") ||
      resource.name.startsWith("projects/kubitus-installer-e2e/global/firewalls/kubitus-") ||
      resource.name.startsWith("projects/kubitus-installer-e2e/regions/europe-west1/addresses/kubitus-") ||
      resource.name.startsWith("projects/kubitus-installer-e2e/regions/europe-west1/forwardingRules/kubitus-") ||
      resource.name.startsWith("projects/kubitus-installer-e2e/regions/europe-west1/subnetworks/kubitus-") ||
      resource.name.startsWith("projects/kubitus-installer-e2e/regions/europe-west1/targetPools/kubitus-") ||
      resource.name.startsWith("projects/kubitus-installer-e2e/zones/europe-west1-b/disks/kubitus-") ||
      resource.name.startsWith("projects/kubitus-installer-e2e/zones/europe-west1-b/instances/kubitus-") ||
      resource.name.startsWith("projects/kubitus-installer-e2e/zones/europe-west1-b/operations/operation-") ||
      resource.name == "projects/kubitus-installer-e2e/zones/europe-west1-b"
      ```

    - Role: `Service Account User`

      No condition
      <!--
      https://issuetracker.google.com/issues/TODO
      Condition `Is kubitus-installer-e2e-gcp@kubitus-installer-e2e.iam.gserviceaccount.com`:

      ```cel
      resource.name == "kubitus-installer-e2e-gcp@kubitus-installer-e2e.iam.gserviceaccount.com"
      ```
      -->

## GKE setup

Create :

- A [role](https://console.cloud.google.com/iam-admin/roles):
  - Title: `Kubitus Installer e2e GKE`
  - Description: `https://gitlab.com/kubitus-project/kubitus-installer/-/blob/main/ci/README.md#gke-setup`
  - ID: `kubitus_installer_e2e_GKE`
  - Role launch stage: `Beta`
  - <summary>
    Permissions:
    <details>

    - `compute.instanceGroupManagers.get`
    - `container.clusters.delete`
    - `container.clusters.get`
    - `container.clusters.update`
    - `container.operations.get`
    </details>
    </summary>
- A [role](https://console.cloud.google.com/iam-admin/roles):
  - Title: `Kubitus Installer e2e GKE global`
  - Description: `https://gitlab.com/kubitus-project/kubitus-installer/-/blob/main/ci/README.md#gke-setup`
  - ID: `kubitus_installer_e2e_GKE_global`
  - Role launch stage: `Beta`
  - <summary>
    Permissions:
    <details>

    - `container.clusters.create`
    </details>
    </summary>
- A [service account](https://console.cloud.google.com/iam-admin/serviceaccounts):
  - Name: `Kubitus Installer e2e GKE`
  - Service account ID: `kubitus-installer-e2e-gke`
  - Description: `https://gitlab.com/kubitus-project/kubitus-installer/-/blob/main/ci/README.md#gke-setup`
  - with a service account key
  - with permissions:
    - Role: `Kubitus Installer e2e GKE`

      No condition
      <!--
      https://issuetracker.google.com/issues/221232296

      Condition `Name starts with kubitus-`:

      ```cel
      resource.name.startsWith("projects/projet-insee-poc/locations/europe-west1-b/clusters/kubitus-") ||
      resource.name.startsWith("projects/projet-insee-poc/zones/europe-west1-b/clusters/kubitus-") ||
      resource.name.startsWith("projects/projet-insee-poc/zones/europe-west1-b/operations/operation-") ||
      resource.name.startsWith("projects/projet-insee-poc/zones/europe-west1-b/instanceGroupManagers/gke-kubitus-")
      ```
      -->

    - Role: `Kubitus Installer e2e GKE global`

      No condition
    - Role: `Service Account User`

      No condition
      <!--
      https://issuetracker.google.com/issues/221232296

      Condition `Is kubitus-installer-e2e-gke@projet-insee-poc.iam.gserviceaccount.com`:

      ```cel
      resource.name == "kubitus-installer-e2e-gke@projet-insee-poc.iam.gserviceaccount.com"
      ```
      -->
    - Role: `Kubernetes Engine Admin`

      No condition
      <!--
      Condition `TODO`:

      ```cel
      ???
      ```
      -->
