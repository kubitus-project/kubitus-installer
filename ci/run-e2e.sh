#!/bin/bash

set -e
set -o pipefail

# =================================================================
# Arguments and variables
# =================================================================
. "$(dirname "$0")/config.sh"

export INVENTORY="$1"
export DIST="$2"
export ANSIBLE_FORCE_COLOR=true
export TERRAFORM_PREFIX="kubitus-$INVENTORY$KUBITUS_CI_PREFIX_EXTRA-$DIST-${CI_COMMIT_SHORT_SHA:-$CI_COMMIT_SHA}"

if [ -z "$INVENTORY" ]; then
  echo 'Missing mandatory parameter: INVENTORY' >&2
  exit 1
fi

if [ -z "$DIST" ]; then
  echo 'Missing mandatory parameter: DIST' >&2
  exit 1
fi

if [ -z "$CI_REPOSITORY_URL" -o -z "$CI_COMMIT_SHA" ]; then
  echo 'Missing one or more of mandatory environment variables: CI_REPOSITORY_URL, CI_COMMIT_SHA' >&2
  exit 1
fi

echo "CI_COMMIT_TIMESTAMP=$CI_COMMIT_TIMESTAMP"
echo "CI_JOB_STARTED_AT=$CI_JOB_STARTED_AT"
CI_COMMIT_AGE="$(($(date -d "$CI_JOB_STARTED_AT" +%s) - $(date -d "$CI_COMMIT_TIMESTAMP" +%s)))"
echo "CI_COMMIT_AGE=$CI_COMMIT_AGE"
if [ "$CI_COMMIT_AUTHOR" = 'Kubitus Bot <kubitus.bot@gmail.com>' -a $CI_COMMIT_AGE -le 180 ]; then
  export CI_PREEMPTIBLE=true
else
  export CI_PREEMPTIBLE=false
fi
echo "CI_PREEMPTIBLE=$CI_PREEMPTIBLE"
# =================================================================
# Functions
# =================================================================
. "$(dirname "$0")/functions.sh"

collect_logs() {
  (
    set -x
    mkdir collected_logs
  )
  for node in $nodes; do
    (
      set -x
      docker cp "$node:/var/log" "collected_logs/$node"
    )
    exec_node \
      "$node" \
      sudo journalctl > "collected_logs/$node/journal.log"
  done
}

# =================================================================
section_start prepare \
  'Prepare'
# =================================================================
set -x
apt-get update
apt-get install -y openssh-client
ssh-keygen  -b 1024 -t rsa -N '' -f $HOME/.ssh/id_rsa

# Get current IP
apt-get install -y curl
export CURRENT_PUBLIC_IP="$(curl https://ipinfo.io/ip)"

# https://docs.ansible.com/ansible/devel/reference_appendices/config.html#avoiding-security-risks-with-ansible-cfg-in-the-current-directory
chmod -R o-w .

set +x

section_end prepare

ret=0
timeout --verbose --kill-after=160m 150m ./ci/run-kubitus.sh || ret=$?

# =================================================================
section_start kubectl_output \
  'kubectl output'
# =================================================================
if [ "$INVENTORY" = 'gke' ]; then
  echo +export KUBECONFIG=inventories/foobar/terraform-gke/kubeconfig >&2
  export KUBECONFIG=inventories/foobar/terraform-gke/kubeconfig
else
  kubespraynode_ip="$(grep ^ansible_host: inventories/foobar/host_vars/kubespraynode/from_terraform.yml | cut -d'"' -f2)" || :
  kubectl() {
    args="ssh ubuntu@$kubespraynode_ip sudo -u kubitus /var/opt/kubitus/config/foobar/kubespray-artifacts/kubectl.sh"
    for arg in "$@"; do
      printf -v arg_escaped "%q" "$arg"
      args="$args $arg_escaped"
    done
    $args
  }
  argocd() {
    args=argocd
    args="sudo -u kubitus KUBECONFIG=/var/opt/kubitus/config/foobar/kubespray-artifacts/admin.conf KUBECTL_EXTERNAL_DIFF=diff\ --unified=10 $args"
    args="ssh ubuntu@$kubespraynode_ip $args"
    for arg in "$@"; do
      printf -v arg_escaped "%q" "$arg"
      args="$args $arg_escaped"
    done
    $args
  }
fi
(
    kubectl get applications -n argocd || :
    kubectl get nodes,all,pv,pvc,ingress -A || :
    kubectl top pods --all-namespaces --containers --sort-by memory --sum > kubectl_top_pods.log || :
    [ "$ret" = 0 ] || {
      kubectl describe applications -A > kubectl_describe_applications.log || :
      kubectl describe nodes,all,pv,pvc,ingress -A > kubectl_describe_all.log || :
      kubectl describe tenant -A > kubectl_describe_tenants.log || :
      ARGOCD_INITIAL_ADMIN_PASSWORD="$(kubectl get secrets -n argocd argocd-initial-admin-secret -o go-template='{{ .data.password | base64decode }}')" || :
      [ -z "$ARGOCD_INITIAL_ADMIN_PASSWORD" ] || {
        argocd --plaintext --port-forward --port-forward-namespace argocd login --username admin --password "$ARGOCD_INITIAL_ADMIN_PASSWORD" || :
        argocd --plaintext --port-forward --port-forward-namespace argocd app list -o name > argocd_app_list.txt || :
        for app in $(cat argocd_app_list.txt); do
          argocd --plaintext --port-forward --port-forward-namespace argocd app diff "$app" > "argocd_app_diff_${app//\//_}.diff" || :
        done
      }
    }
    while read -r namespace pod remaining
    do
      echo "Retrieving logs for $namespace/$pod"
      kubectl logs -n "$namespace" "$pod" --all-containers --max-log-requests=10 > "kubectl_logs_${namespace}_$pod.log" || :
    done < <(kubectl get po --all-namespaces --no-headers | grep -v ' Completed ' | grep -v ' 1/1 ' | grep -v ' 2/2 ' | grep -v ' 3/3 ' | grep -v ' 4/4 ' | grep -v ' 5/5 ' | grep -v ' 6/6 ' | grep -v ' 7/7 ' || :)
)
section_end kubectl_output

if [ "$INVENTORY" = 'gke' ]; then
  kubitus_keep_name=gitlabnode
  kubitus_keep_ip="$(grep ^ansible_host: "inventories/foobar/host_vars/$kubitus_keep_name/from_terraform.yml" | cut -d'"' -f2)" || :
else
  kubitus_keep_name=kubespraynode
  kubitus_keep_ip="$kubespraynode_ip"
fi
# =================================================================
section_start kubitus_keep \
  "Suspend job, while /tmp/kubitus_keep on $kubitus_keep_name is present"
# =================================================================
retry=0
max_retries=30
retry_interval=60

until [ ${retry} -ge ${max_retries} ]
do
  ssh -o StrictHostKeyChecking=no "ubuntu@$kubitus_keep_ip" test -f /tmp/kubitus_keep || break
  retry="$((${retry}+1))"
  echo "/tmp/kubitus_keep present, retrying in ${retry_interval} seconds (${retry}/${max_retries})"
  sleep "$retry_interval"
done

section_end kubitus_keep

# collect_logs

# =================================================================
section_start terraform_destroy \
  'Terraform destroy'
# =================================================================
# Come back to an always working DNS
echo 'nameserver 8.8.8.8' > /etc/resolv.conf

retry=0
max_retries=10
retry_interval=10

until [ ${retry} -ge ${max_retries} ]
do
  ansible-playbook external_components.yml --tags terraform_destroy -e terraform_apply_mode=destroy && break || :
  retry="$((${retry}+1))"
  echo "Terraform destroy failed, retrying in ${retry_interval} seconds (${retry}/${max_retries})"
  sleep "$retry_interval"
done

if [ ${retry} -ge ${max_retries} ]; then
  echo "Terraform destroy failed after ${max_retries} attempts!"
  ret=1
fi

section_end terraform_destroy

exit $ret
