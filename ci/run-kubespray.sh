#!/bin/sh

set -e

export ANSIBLE_FORCE_COLOR=true

cd /opt/kubitus/kubespray
. venv/bin/activate

ansible-playbook \
  -i inventory/foobar/ \
  --become \
  --user ubuntu \
  cluster.yml

deactivate
