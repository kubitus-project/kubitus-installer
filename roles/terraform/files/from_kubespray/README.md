# From kubespray

This directory contains copies from [Kubespray](https://github.com/kubernetes-sigs/kubespray):

- [contrib/terraform/gcp](https://github.com/kubernetes-sigs/kubespray/tree/6d683c98a3204bcc43e2707a1123d866081ed07d/contrib/terraform/gcp)
- [contrib/terraform/vsphere](https://github.com/kubernetes-sigs/kubespray/tree/18efdc2c51c5881c8647c06d02f8b505c5712876/contrib/terraform/vsphere)
