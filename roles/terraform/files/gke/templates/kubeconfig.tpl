apiVersion: v1
kind: Config
current-context: admin
contexts:
- context:
    cluster: ${cluster_name}
    namespace: default
    user: admin
  name: admin
clusters:
- cluster:
    server: https://${endpoint}
    certificate-authority-data: ${cluster_ca_certificate}
  name: ${cluster_name}
users:
- name: admin
  user:
    token: ${access_token}

