variable "gcp_project_id" {
  description = "ID of the project"
  type        = string
}

variable "keyfile_location" {
  description = "Location of the json keyfile to use with the google provider"
  type        = string
}

variable "location" {
  description = "Location (region or zone) of the cluster"
  type        = string
}

variable "deletion_protection" {
  description = "Prevent Terraform from destroying the cluster"
  type        = bool
  default     = true
}

variable "prefix" {
  description = "Prefix for resource names"
  default     = "default"
}

variable "release_channel" {
  description = "Release channel"
  default     = "REGULAR"
}

variable "network" {
  description = "Network"
  default     = "default"
}

variable "subnetwork" {
  description = "Subnet"
  default     = "default"
}

variable "node_pools" {
  description = "Container node pools"

  type = map(object({
    location = string

    initial_node_count = number
    min_node_count = number
    max_node_count = number

    machine_type = string
    preemptible  = bool

    disk_size_gb = number
  }))
}

variable "sa_email" {
  type    = string
  default = ""
}

variable "sa_scopes" {
  type    = list(string)
  default = ["https://www.googleapis.com/auth/cloud-platform"]
}

variable "kubeconfig" {
  description = "KUBECONFIG file"
  default     = "kubeconfig"
}
