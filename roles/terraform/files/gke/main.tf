terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 6.0"
    }
  }
}

provider "google" {
  credentials = file(var.keyfile_location)
  project     = var.gcp_project_id
}

resource "google_container_cluster" "primary" {
  name     = var.prefix
  location = var.location

  # Prevent Terraform from destroying the cluster if true
  deletion_protection = var.deletion_protection

  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true
  # initial_node_count       = 1

  node_pool {
    name = "default-pool"

    autoscaling {
      min_node_count = 0
      max_node_count = 1
    }

    node_config {
      service_account = var.sa_email
      oauth_scopes    = var.sa_scopes
    }
  }

  release_channel {
    channel = var.release_channel
  }

  network_policy {
    enabled = true
  }

  addons_config {
    http_load_balancing {
      disabled = true
    }
  }

  lifecycle {
    ignore_changes = [
      node_pool
    ]
  }

  network = var.network
  subnetwork = var.subnetwork
}

resource "google_container_node_pool" "node_pool" {
  for_each = {
    for name, node_pool in var.node_pools :
    name => node_pool
  }

  name     = "${var.prefix}-${each.key}"
  cluster  = google_container_cluster.primary.name
  location = each.value.location

  initial_node_count = each.value.initial_node_count

  lifecycle {
    ignore_changes = [
      initial_node_count
    ]
  }

  autoscaling {
    min_node_count = each.value.min_node_count
    max_node_count = each.value.max_node_count
  }

  node_config {
    machine_type = each.value.machine_type
    preemptible  = each.value.preemptible

    disk_size_gb = each.value.disk_size_gb

    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    service_account = var.sa_email
    oauth_scopes    = var.sa_scopes
  }
}

data "google_client_config" "default" {
  depends_on = [google_container_cluster.primary]
}


data "template_file" "kubeconfig" {
  template = file("${path.module}/templates/kubeconfig.tpl")
  vars = {
    cluster_name           = google_container_cluster.primary.name,
    endpoint               = google_container_cluster.primary.endpoint,
    cluster_ca_certificate = google_container_cluster.primary.master_auth.0.cluster_ca_certificate,
    access_token           = data.google_client_config.default.access_token,
  }
}

resource "local_file" "kubeconfig" {
  content  = data.template_file.kubeconfig.rendered
  filename = var.kubeconfig
  file_permission = "0600"
}
