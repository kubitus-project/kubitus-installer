#!/bin/bash

set -e

kubectl=$(ls /var/opt/kubitus/config/cluster-*/kubespray-artifacts/kubectl | head -1)

error=0
echo -e "#############################\n\
 Test if Gatekeeper is alive \n\
#############################"
$kubectl wait -n gatekeeper deploy --all --for=condition=Available=true --timeout=10s || error=1

if [[ $error == 0 ]]; then
	echo "No problem detected"
	false
else
	echo -en "\nWould you want to initiate the recovering ?(Y/n) "
	while [[ $response != "y" ]];do
		read response
		case $response in
			y|Y|"yes"|"Yes"|"") response="y";;
			n|N|"no"|"No") false;;
			*) continue;;
		esac
	done
fi

echo -e "####################\n\
 Delete ArgoCD pods \n\
####################"

$kubectl delete pods -n argocd --all --timeout=30s || :

loop=0
while [[ $loop < 4 ]]; do
	loop=$(($loop+1))
	error=0

	echo -e "####################################\n\
 Delete webhook and Gatekeeper pods \n\
####################################"

	$kubectl delete validatingwebhookconfigurations.admissionregistration.k8s.io/gatekeeper-validating-webhook-configuration  \
	mutatingwebhookconfigurations.admissionregistration.k8s.io/gatekeeper-mutating-webhook-configuration || :

	$kubectl delete pods -n gatekeeper --all || :

	echo -e "#########################\n\
 Wait of Gatekeeper pods \n\
#########################"

	time $kubectl wait -n gatekeeper deploy --all --for=condition=Available=true --timeout=5m || if [[ $loop == 4 ]]; then \
		echo -e "\n\n-------------------------------\n\
 Gatekeeper is not operational \n\
-------------------------------"; error=1; fi
	if [[ $error == 0 ]]; then
		break
	else
		echo -e "--------------------\n\		
 New attempt \n--------------------"
	fi
done

echo -e "###########################\n\
 Deleting Kube-system pods \n\
###########################"

$kubectl delete pods -n kube-system --all

sleep 10
echo -e "##########################\n\
 Wait of Kube-system pods \n\
##########################"

time $kubectl wait -n kube-system pods --all --for=condition=Ready=true --timeout=5m

echo -e "##################################################################\n\
 Delete others namespaces pods \n\
##################################################################"

for ns in $($kubectl get namespaces --no-headers --output custom-columns=NAME:.metadata.name); do
	if ! [[ "$ns" =~ ^(gatekeeper|kube-system)$ ]]; then
		$kubectl delete pods -n $ns --all --wait=false
	fi
done

echo -e "####################################################\n\
 The cluster should be operational in a few minutes \n\
####################################################"
