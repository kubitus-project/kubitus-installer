- name: Download file
  tags:
  - download
  - download_download-file
  when:
  - download.enabled | default(True)
  block:

# download_name:
# download:
#   enabled:
#   url:
#   gitlab_project:
#   package_name:
#   package_version:
#   file_name:
#   mode:
#   sha256:
#   extract:
#   creates: # Mandatory when extract == true
#   dest:

  - name: Download file | Set facts
    set_fact:
      # yamllint disable rule:line-length
      file_url: >-
        {%- if offline_enabled -%}
          https://{{ gitlab_fqdn }}/api/v4/projects/{{ download.gitlab_project | urlencode | replace('/', '%2F') }}/packages/generic/{{ download.package_name }}/{{ download.package_version }}/{{ download.file_name | default(download.url | basename) }}
        {%- else -%}
          {{ download.url }}
        {%- endif -%}
      cache_file_path: "{{ download_cache_dir }}/{{ download.package_name }}/{{ download.package_version }}/{{ download.file_name | default(download.url | basename) }}"
      # yamllint enable rule:line-length
      dest: "{{ download.dest | default(None) }}"


  - name: Download file | Summary
    debug:
      msg:
        url: "{{ file_url }}"
        cache: "{{ cache_file_path }}"
        dest: "{{ dest }}"


  - name: Download file | Create cache directory
    file:
      path: "{{ cache_file_path | dirname }}"
      owner: "{{ kubectl_user }}"
      group: "{{ kubectl_group }}"
      mode: 0755
      state: directory
    become: "{{ kubectl_become }}"
    become_user: "{{ kubectl_user }}"


  - name: Download file | Download to cache
    get_url:
      url: "{{ file_url }}"
      dest: "{{ cache_file_path }}"
      mode: "{{ download.mode | default(omit) }}"
      checksum: "{{ 'sha256:' + download.sha256 if download.sha256 | default(False) else omit }}"
    register: get_url_result
    until: "'OK' in get_url_result.msg or
            'file already exists' in get_url_result.msg or
            'Not Modified' in get_url_result.msg"
    retries: 4
    delay: 5
    environment: "{{ proxy_env }}"
    become: "{{ kubectl_become }}"
    become_user: "{{ kubectl_user }}"


  - name: Download file | Create extraction directory
    file:
      path: "{{ cache_file_path }}-extracted"
      owner: "{{ kubectl_user }}"
      group: "{{ kubectl_group }}"
      mode: 0755
      state: directory
    when:
    - download.extract|default(false)
    become: "{{ kubectl_become }}"
    become_user: "{{ kubectl_user }}"


  - name: Download file | Extract
    unarchive:
      src: "{{ cache_file_path }}"
      dest: "{{ cache_file_path }}-extracted"
      creates: "{{ cache_file_path }}-extracted/{{ download.creates }}"
      mode: "{{ download.mode | default(omit) }}"
      copy: false
      extra_opts: "{{
        ['--strip-components=' + (download.strip_components | default(1) | string)]
        if not cache_file_path.endswith('.zip')
        else []
        }}"
    when:
    - download.extract|default(false)
    become: "{{ kubectl_become }}"
    become_user: "{{ kubectl_user }}"


  - name: Download file | Create dest directory
    file:
      path: "{{ download.dest | dirname }}"
      mode: 0755
      state: directory
    when:
    - not not dest


  - name: Download file | Symlink
    file:
      state: link
      src: "{{ cache_file_path }}{% if download.extract | default(false) %}-extracted/{{ download.creates }}{% endif %}"
      dest: "{{ download.dest }}"
    become: true
    when:
    - not not dest
