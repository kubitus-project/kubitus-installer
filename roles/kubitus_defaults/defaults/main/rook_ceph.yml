# GitOps
rook_ceph_enabled: true
rook_ceph_description: Rook Ceph
rook_ceph_namespace_pod_security: privileged
rook_ceph_helm_dependencies:
- name: rook-ceph
  version: "{{ rook_ceph_chart_version }}"
  repository: "{% if offline_enabled
    %}https://{{ gitlab_fqdn }}/api/v4/projects/external-packages%2Frook-ceph/packages/helm/stable/{%
    else
    %}https://charts.rook.io/release{% endif %}"
- name: rook-ceph-cluster
  version: "{{ rook_ceph_cluster_chart_version }}"
  repository: "{% if offline_enabled
    %}https://{{ gitlab_fqdn }}/api/v4/projects/external-packages%2Frook-ceph/packages/helm/stable/{%
    else
    %}https://charts.rook.io/release{% endif %}"
rook_ceph_server_side_apply: true  # Workaround for https://gitlab.com/kubitus-project/kubitus-installer/-/merge_requests/1958#note_1318344569
rook_ceph_extra_cluster_resource_whitelist:
- group: storage.k8s.io
  kind: StorageClass
- group: snapshot.storage.k8s.io
  kind: VolumeSnapshotClass
rook_ceph_extra_namespace_resource_whitelist:
- group: ceph.rook.io
  kind: CephBlockPool
- group: ceph.rook.io
  kind: CephCluster
- group: ceph.rook.io
  kind: CephFilesystem
- group: ceph.rook.io
  kind: CephFilesystemSubVolumeGroup
- group: ceph.rook.io
  kind: CephObjectStore


rook_ceph_cephfs_enabled: false
rook_ceph_objectstore_enabled: false

rook_ceph_storage_use_all_nodes: true
rook_ceph_storage_use_all_devices: false
rook_ceph_storage_device_filter: '^sd[^a-e]'

rook_ceph_remove_osds_if_out_and_safe_to_remove: false

rook_ceph_mgr_dashboard_fqdn: "rook-ceph.{{ top_level_domain }}"
rook_ceph_mgr_dashboard_ingress_certificate_issuer: "{{ default_ingress_certificate_issuer }}"

rook_ceph_operator_memory_request:
rook_ceph_operator_cpu_request:
rook_ceph_operator_memory_limit:
rook_ceph_operator_cpu_limit:

rook_ceph_toolbox_memory_request:
rook_ceph_toolbox_cpu_request:
rook_ceph_toolbox_memory_limit:
rook_ceph_toolbox_cpu_limit:

rook_ceph_cluster_spec_resource_overrides: {}

rook_ceph_chart_version: v1.16.1
rook_ceph_cluster_chart_version: v1.16.1

rook_ceph_operator_image: docker.io/rook/ceph:v1.16.1@sha256:d749ab4fcad255b4d114f481a4f86dddd8c7910f4840d49b1a10be1190d1eb1c
rook_ceph_ceph_image: quay.io/ceph/ceph:v19.2.0@sha256:200087c35811bf28e8a8073b15fa86c07cce85c575f1ccd62d1d6ddbfdc6770a

# CSI
rook_ceph_cephcsi_image: quay.io/cephcsi/cephcsi:v3.13.0@sha256:5f22a62f61fbae57e873e2b3da19d5c9bc553b786e20413286d2e4fda6c77fd2
rook_ceph_csi_registrar_image: registry.k8s.io/sig-storage/csi-node-driver-registrar:v2.12.0@sha256:0d23a6fd60c421054deec5e6d0405dc3498095a5a597e175236c0692f4adee0f
rook_ceph_csi_resizer_image: registry.k8s.io/sig-storage/csi-resizer:v1.12.0@sha256:ab774734705a906561e15b67f6a96538f3ad520335d636f793aaafe87a3b5200
rook_ceph_csi_provisioner_image: registry.k8s.io/sig-storage/csi-provisioner:v5.1.0@sha256:672e45d6a55678abc1d102de665b5cbd63848e75dc7896f238c8eaaf3c7d322f
rook_ceph_csi_snapshotter_image: registry.k8s.io/sig-storage/csi-snapshotter:v8.2.0@sha256:dd788d79cf4c1b8edee6d9b80b8a1ebfc51a38a365c5be656986b129be9ac784
rook_ceph_csi_attacher_image: registry.k8s.io/sig-storage/csi-attacher:v4.7.0@sha256:6e54dae32284f60a2de1645c527715e4c934e5ce7899560c0d845bac957118dc


rook_ceph_projects:
  external-packages/rook-ceph:
    extends: .packages
    package_mirrors:
    - helm:
        repo_url: https://charts.rook.io/release
        package_name: rook-ceph
        versions:
        - "{{ rook_ceph_chart_version }}"
    - helm:
        repo_url: https://charts.rook.io/release
        package_name: rook-ceph-cluster
        versions:
        - "{{ rook_ceph_cluster_chart_version }}"

  external-registries/docker.io:
    extends: .registry
    image_mirrors:
    - from: "{{ rook_ceph_operator_image }}"
    - from: "{{ rook_ceph_ceph_image }}"

  external-registries/registry.k8s.io:
    extends: .registry
    image_mirrors:
    - from: "{{ rook_ceph_csi_registrar_image }}"
    - from: "{{ rook_ceph_csi_resizer_image }}"
    - from: "{{ rook_ceph_csi_provisioner_image }}"
    - from: "{{ rook_ceph_csi_snapshotter_image }}"
    - from: "{{ rook_ceph_csi_attacher_image }}"

  external-registries/quay.io:
    extends: .registry
    image_mirrors:
    - from: "{{ rook_ceph_cephcsi_image }}"
