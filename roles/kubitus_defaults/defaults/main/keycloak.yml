# GitOps
keycloak_enabled: false
keycloak_description: KeyCloak Operator
keycloak_namespace: keycloak-operator
keycloak_extra_namespaces:
- grafana
- keycloak
keycloak_helm_dependencies:
- name: keycloak-operator
  version: "{{ keycloak_operator_chart_version }}"
  repository: "{% if offline_enabled
    %}https://{{ gitlab_fqdn }}/api/v4/projects/external-packages%2Fkeycloak-operator/packages/helm/stable/{%
    else
    %}https://gitlab.com/api/v4/projects/50848788/packages/helm/stable{% endif %}"
- name: keycloak-operator-legacy
  version: "{{ keycloak_operator_legacy_chart_version }}"
  repository: "{% if offline_enabled
    %}https://{{ gitlab_fqdn }}/api/v4/projects/external-packages%2Fkeycloak-operator-legacy/packages/helm/stable/{%
    else
    %}https://gitlab.com/api/v4/projects/50776050/packages/helm/stable{% endif %}"
keycloak_extra_cluster_resource_whitelist:
- group: mutations.gatekeeper.sh
  kind: Assign
keycloak_extra_namespace_resource_whitelist:
- group: postgresql.cnpg.io
  kind: Cluster
- group: k8s.keycloak.org
  kind: Keycloak
- group: k8s.keycloak.org
  kind: KeycloakRealmImport
- group: keycloak.org
  kind: Keycloak
- group: keycloak.org
  kind: KeycloakRealm
- group: keycloak.org
  kind: KeycloakClient
keycloak_sealed_secrets:
- keycloak-tls
- keycloak-bootstrap-admin
- credential-keycloak-legacy
keycloak_sealed_secret_keycloak_tls_namespace: keycloak
keycloak_sealed_secret_keycloak_tls_type: kubernetes.io/tls
keycloak_sealed_secret_keycloak_tls_items:
- ca.crt
- tls.key
- tls.crt
keycloak_sealed_secret_keycloak_tls_data_ca_crt_value:
  "{{ lookup('file', pki_dir + '/intermediate_ca_signed.pem') }}"
keycloak_sealed_secret_keycloak_tls_data_tls_key_value:
  "{{ lookup('file', pki_dir + '/certificates/' + gitops_instance_variables.fqdn + '/cert-key.pem') }}"
keycloak_sealed_secret_keycloak_tls_data_tls_crt_value:
  "{{ lookup('file', pki_dir + '/certificates/' + gitops_instance_variables.fqdn + '/cert.pem') }}"
keycloak_sealed_secret_keycloak_bootstrap_admin_namespace: keycloak
keycloak_sealed_secret_keycloak_bootstrap_admin_items:
- username
- password
keycloak_sealed_secret_keycloak_bootstrap_admin_type: kubernetes.io/basic-auth
keycloak_sealed_secret_keycloak_bootstrap_admin_data_username_value: admin
keycloak_sealed_secret_keycloak_bootstrap_admin_data_password_value: "{{ gitops_instance_variables.admin_password }}"
keycloak_sealed_secret_credential_keycloak_legacy_namespace: keycloak
keycloak_sealed_secret_credential_keycloak_legacy_items:
- ADMIN_USERNAME
- ADMIN_PASSWORD
keycloak_sealed_secret_credential_keycloak_legacy_data_admin_username_value: admin
keycloak_sealed_secret_credential_keycloak_legacy_data_admin_password_value: "{{ gitops_instance_variables.admin_password }}"


keycloak_postgresql_image: "{{ cloudnative_pg_postgresql_image }}"
keycloak_postgresql_instances: 2
keycloak_admin_password:
  "{{ lookup('password',
             kubitus_credentials_dir +
             '/keycloak_admin_password.creds length=32 chars=ascii_letters,digits') }}"

keycloak_fqdn: "auth.{{ top_level_domain }}"
keycloak_ingress_certificate_issuer: "{{ default_ingress_certificate_issuer }}"

keycloak_identity_providers: []

keycloak_operator_chart_version: 1.0.20250221141800
keycloak_operator_image: quay.io/keycloak/keycloak-operator:26.1.3@sha256:76bca3c12b7bdac8690d6293914d546cdb89c14d2545c76ab01e88f73086983d
keycloak_image: quay.io/keycloak/keycloak:26.1.3@sha256:2ce6c7c70994c70dbbd70b372a5422c3b4eebb32583175eac03751320609e52c
keycloak_operator_legacy_chart_version: 1.0.0
keycloak_operator_legacy_image: quay.io/keycloak/keycloak-operator:19.0.3-legacy@sha256:09d52508fee909e3d0519026960ae6f4ee34d09fa1a369a5e528fe4a95905835

keycloak_projects:
  external-packages/keycloak-operator:
    extends: .packages
    package_mirrors:
    - helm:
        repo_url: https://gitlab.com/api/v4/projects/50848788/packages/helm/stable
        package_name: keycloak-operator
        versions:
        - "{{ keycloak_operator_chart_version }}"
  external-packages/keycloak-operator-legacy:
    extends: .packages
    package_mirrors:
    - helm:
        repo_url: https://gitlab.com/api/v4/projects/50776050/packages/helm/stable
        package_name: keycloak-operator-legacy
        versions:
        - "{{ keycloak_operator_legacy_chart_version }}"

  external-registries/quay.io:
    extends: .registry
    image_mirrors:
    - from: "{{ keycloak_operator_image }}"
    - from: "{{ keycloak_image }}"
    - from: "{{ keycloak_operator_legacy_image }}"
