# GitOps
loki_enabled: true
loki_helm_dependencies:
- name: loki
  version: "{{ loki_chart_version }}"
  repository: "{% if offline_enabled
    %}https://{{ gitlab_fqdn }}/api/v4/projects/external-packages%2Floki/packages/helm/stable/{%
    else
    %}https://grafana.github.io/helm-charts{% endif %}"
loki_helm_skip_crds: true
loki_sealed_secrets:
- kubitus-basic-auth
loki_sealed_secret_kubitus_basic_auth_items:
- .htpasswd
loki_sealed_secret_kubitus_basic_auth_data__htpasswd_value:
  "{{ lookup('file', kubitus_credentials_dir + '/loki_instance_default_htpasswd.creds') }}"


loki_retention_enabled: true
loki_retention_period: 744h  # 31d
loki_split_queries_by_interval: 15m
loki_ingestion_rate_mb: 4
loki_ingestion_burst_size_mb: 6
loki_max_query_length: 721h
loki_max_query_parallelism:
loki_results_cache_allocated_memory_mb: 1024
loki_chunks_cache_allocated_memory_mb: 8192

loki_max_outstanding_requests_per_tenant:

loki_max_outstanding_per_tenant:

loki_schemaconfig_v13_from: "2024-06-04"
loki_schemaconfigs:
- from: "2022-01-11"
  index:
    prefix: loki_index_
    period: 24h
  object_store: filesystem
  schema: v12
  store: boltdb-shipper
- from: "{{ loki_schemaconfig_v13_from }}"
  store: tsdb
  index:
    prefix: loki_index_
    period: 24h
  object_store: filesystem
  schema: v13

loki_write_persistence_size: 10Gi
loki_read_persistence_size: 10Gi
loki_single_binary_persistence_size: 40Gi
loki_kube_rbac_proxy_enabled: false


# Resources
loki_gateway_memory_request:
loki_gateway_cpu_request:
loki_gateway_memory_limit:
loki_gateway_cpu_limit:

loki_write_memory_request:
loki_write_cpu_request:
loki_write_memory_limit:
loki_write_cpu_limit:

loki_read_memory_request:
loki_read_cpu_request:
loki_read_memory_limit:
loki_read_cpu_limit:

loki_single_binary_memory_request: 1Gi
loki_single_binary_cpu_request: 10m
loki_single_binary_memory_limit: 12Gi
loki_single_binary_cpu_limit:


# Artifacts
loki_chart_version: 6.27.0
loki_kubectl_image: docker.io/bitnami/kubectl:1.32.2@sha256:9933302d768bbb344fedae9e6e1e452beaf63ae231f3f328272032455ea9aa1e
loki_image: docker.io/grafana/loki:3.4.2@sha256:58a6c186ce78ba04d58bfe2a927eff296ba733a430df09645d56cdc158f3ba08
loki_gateway_image: docker.io/nginxinc/nginx-unprivileged:1.27-alpine@sha256:3578209bf946becc0d3bfae25b25c4f97f18cfb5f42ab85239ae15d3d7a8a9bb
loki_memcached_image: docker.io/library/memcached:1.6.37-alpine@sha256:4fb13e4757387000230be124c5a74f107abfd1f65e2526f4c627110d666bf5a3
loki_memcached_exporter_image: docker.io/prom/memcached-exporter:v0.15.1@sha256:bc441feeb97304c7af70c76ee465080c9e071665560bfbd31cd24318c202ed18
loki_sidecar_image: docker.io/kiwigrid/k8s-sidecar:1.30.1@sha256:439c5ac985827e6a85ad4490a66320b62aefd9e8c6d06fdb1d9c83d582f8a902
loki_kube_rbac_proxy_image:
  quay.io/brancz/kube-rbac-proxy:v0.19.0@sha256:c2ef3c3fcb4ffcfab30088b19c8ef18931aac8cf072c75ef72c654457e1d632b


loki_projects:
  external-packages/loki:
    extends: .packages
    package_mirrors:
    - helm:
        repo_url: https://grafana.github.io/helm-charts
        package_name: loki
        versions:
        - "{{ loki_chart_version }}"

  external-registries/docker.io:
    extends: .registry
    image_mirrors:
    - from: "{{ loki_kubectl_image }}"
    - from: "{{ loki_image }}"
    - from: "{{ loki_gateway_image }}"
    - from: "{{ loki_memcached_image }}"
    - from: "{{ loki_memcached_exporter_image }}"
    - from: "{{ loki_sidecar_image }}"

  external-registries/quay.io:
    extends: .registry
    image_mirrors:
    - from: "{{ loki_kube_rbac_proxy_image }}"
      enabled: "{{ loki_kube_rbac_proxy_enabled }}"
